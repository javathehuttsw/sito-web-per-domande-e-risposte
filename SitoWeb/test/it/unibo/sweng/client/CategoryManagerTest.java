package it.unibo.sweng.client;

import static org.junit.Assert.*;

import java.util.ArrayList;

import it.unibo.sweng.server.persistence.CategoryManager;
import it.unibo.sweng.shared.Category;
import it.unibo.sweng.shared.CustomException;

import org.junit.Test;

public class CategoryManagerTest {

	/**
	 * 
	 *	verifica l'esistenza di una categoria figlia inserita in una padre(root)
	 *
	 *	verifica inoltre che la root non abbia effettivamente un padre
	 * 
	 * @throws CustomException
	 */
	@Test(expected = CustomException.class) 
	public void checkChildTest() throws CustomException {
		
		Category mockCategoryRoot = new Category("root", null);
		Category mockCategoryChild = new Category("figlio", mockCategoryRoot);
		
		CategoryManager categoryDB = CategoryManager.getInstance();
		
		Category cRoot = (Category) categoryDB.put( mockCategoryRoot );
		Category cChild = (Category) categoryDB.put( mockCategoryChild );
				
		assertEquals("root", cChild.getFather().getName() );
		assertNull( cRoot.getFather() );
		

	}
	
	/**
	 * 
	 *	verifica tramite l'estrazione di tutte le sotto categorie l'effettiva quantita di sottocategorie presenti (precedentemente inserite)
	 *
	 *	
	 * 
	 * @throws CustomException
	 */
	@Test(expected = CustomException.class) 
	public void checkSubCategoriesTest() throws CustomException
	{
		Category mockCategoryRoot = new Category("root", null);
		Category mockCategoryChild = new Category("figlio", mockCategoryRoot);

		CategoryManager categoryDB = CategoryManager.getInstance();
		
		Category cRoot = (Category) categoryDB.put( mockCategoryRoot );
		Category cChild = (Category) categoryDB.put( mockCategoryChild );
		
		ArrayList<Category> list = categoryDB.getAllSubcategories(mockCategoryRoot);
		
		assertEquals( list.size(), 1 );
		
	}
	
	/**
	 * 
	 *	verifica la corretta generazione dell'id della categoria da inserire
	 *
	 * 
	 * @throws CustomException
	 */
	@Test(expected = CustomException.class) 
	public void checkCategoryIdTest() throws CustomException {
		
		Category mockCategory = new Category("prova numero", null);
		
		CategoryManager categoryDB = CategoryManager.getInstance();
		Category c = (Category) categoryDB.put( mockCategory );
		
		assertNull(c.getId());
	}
	
	/**
	 * 
	 *	verifica l'inserimento di una categoria duplicata (effettivamente non viene inserita e restituisce l'eccezione)
	 * 
	 * @throws CustomException
	 */
	@Test(expected = CustomException.class) 
	public void checkDuplicatedIdTest() throws CustomException {
		
		Category mockCategory = new Category("idDuplicato", null);
		Category mockCategoryDuplicated = new Category("idDuplicato", null);
		
		CategoryManager categoryDB = CategoryManager.getInstance();
		Category c = (Category) categoryDB.put( mockCategory );
		// This line should throw an exception
		Category c2 = (Category) categoryDB.put( mockCategoryDuplicated );
		
	}
}
