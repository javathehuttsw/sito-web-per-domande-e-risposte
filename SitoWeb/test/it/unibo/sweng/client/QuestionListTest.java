package it.unibo.sweng.client;

import java.util.ArrayList;

import org.junit.Test;
import static org.junit.Assert.*;

import it.unibo.sweng.server.persistence.CategoryManager;
import it.unibo.sweng.server.persistence.QuestionManager;
import it.unibo.sweng.server.persistence.UserManager;
import it.unibo.sweng.shared.Category;
import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.Question;
import it.unibo.sweng.shared.User;

public class QuestionListTest {

	/**
	 * 
	 *	inserisce la domanda e verifica che sia stata inserita e che la lista delle domande non sia null
	 *
	 * @throws CustomException
	 */
	@Test(expected = CustomException.class) 
	public void testListQuestion() throws CustomException
	{		
		Category category = new Category("informatica",null);
		CategoryManager categoryDB = CategoryManager.getInstance();
		Category c = (Category) categoryDB.put( category );

		User user = new User("prova92","prova@prova.com","admin");
		UserManager userDB = new UserManager();
		User u = (User) userDB.put( user );
				
		Question question = new Question("domandone",c,u);
		QuestionManager questionDB = new QuestionManager();
		Question q = (Question) questionDB.put( question );
		
		ArrayList<Category> lc = new ArrayList<Category>();
		lc.add(c);
		ArrayList<Question> lq = questionDB.get(lc);
		
		assertNotNull(lq);
		assertEquals(lq.get(1).getText(),q.getText());
	}

}
