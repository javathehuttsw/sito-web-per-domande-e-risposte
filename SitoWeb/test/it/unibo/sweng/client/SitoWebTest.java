package it.unibo.sweng.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;

import it.unibo.sweng.client.services.SitoWebService;
import it.unibo.sweng.client.services.SitoWebServiceAsync;

public class SitoWebTest extends GWTTestCase{

	@Override
	public String getModuleName() {

		return "it.unibo.sweng.SitoWeb";
	}

	public void testSimple() {                                         
		assertTrue (true);
	}
	
	public void testSetup()
	{
		SitoWebServiceAsync service = GWT.create(SitoWebService.class);
		
		service.setup(new AsyncCallback<Boolean>() {
			
			@Override
			public void onSuccess(Boolean result) {
				
				//se è true non ci sono errori durante il setup
				assertTrue(result);
				
				finishTest();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
}
