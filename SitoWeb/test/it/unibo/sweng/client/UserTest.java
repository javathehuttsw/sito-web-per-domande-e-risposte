package it.unibo.sweng.client;

import java.util.HashMap;
import java.util.Map;

import it.unibo.sweng.server.persistence.UserManager;
import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.User;

import org.junit.Test;
import static org.junit.Assert.*;

public class UserTest{
	
	/**
	 * 
	 * Creazione utente con parametri obbligatori
	 * e opzionali, verifico che sia corretta l'istanza
	 * 
	 */
	public void testRegistration(){
		
		String username = "RobertTheKing";
		String password = "baratheon";
		String email = "robert@baratheon.com";
		
		Map<String, String> data = new HashMap<String, String>();
		
		data.put("username",username);
		data.put("password",password);
		data.put("email",email);
		
		
		User necessaryUser = new User(data);
		
		assertNotNull (necessaryUser);
		
		assertEquals (username,necessaryUser.getUsername());
		assertEquals (password,necessaryUser.getPassword());
		assertEquals (email,necessaryUser.getEmail());
		
		assertNull (necessaryUser.getHome());
		
		String name = "Robert";
		String lastName = "Baratheon";
		String gender = "Male";
		String bornDate = "01-01-1980";
		String bornPlace = "Roccia del drago";
		String home = "Approdo del re";
		
		data.put("name", name);
		data.put("lastName", lastName);
		data.put("gender", gender);
		data.put("bornDate", bornDate);
		data.put("bornPlace", bornPlace);
		data.put("home", home);
		
		User optionalUser = new User(data);
		
		assertNotNull (optionalUser);
		
		assertEquals (name, optionalUser.getName());
		assertEquals (lastName, optionalUser.getLastName());
		assertEquals (gender,optionalUser.getGender());
		assertEquals (bornDate, optionalUser.getBornDate());
		assertEquals (bornPlace, optionalUser.getBornPlace());
		assertEquals (home, optionalUser.getHome());
		
	}
	
	/**
	 * 
	 * 
	 * Creo e un utente giudice
	 * 
	 * @throws CustomException
	 */
	@Test(expected = CustomException.class) 
	public void testJudge() throws CustomException{
		
		User user = new User("prova92","prova@prova.com","giudice");
		
		UserManager userDB = new UserManager();
		
		User u = userDB.put( user );
		
		assertNotNull (u.getType());
		
		assertTrue (u.isJudge());

	}
}
