package it.unibo.sweng.client;

import static org.junit.Assert.*;

import java.util.ArrayList;

import it.unibo.sweng.server.persistence.AnswerManager;
import it.unibo.sweng.server.persistence.CategoryManager;
import it.unibo.sweng.server.persistence.QuestionManager;
import it.unibo.sweng.server.persistence.UserManager;
import it.unibo.sweng.shared.Answer;
import it.unibo.sweng.shared.Category;
import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.Judgement;
import it.unibo.sweng.shared.Question;
import it.unibo.sweng.shared.User;

import org.junit.Test;

public class AnswerManagerTest {


	/**
	 * 
	 * Verifica che sia stata inserita la risposta
	 * 
	 * verifica inoltre: categoria, utente, question
	 * 
	 * @throws CustomException
	 */
	@Test(expected = CustomException.class) 
	public void checkAnswer() throws CustomException {
		
		Category category = new Category("informatica",null);
		CategoryManager categoryDB = CategoryManager.getInstance();
		Category c = (Category) categoryDB.put( category );

		User user = new User("prova92","prova@prova.com","admin");
		UserManager userDB = new UserManager();
		User u = (User) userDB.put( user );
				
		Question question = new Question("domandone",c,u);
		QuestionManager questionDB = new QuestionManager();
		Question q = (Question) questionDB.put( question );
		
		Answer answer = new Answer("che tipo di domandone",q,u);
		AnswerManager answerDB = AnswerManager.getInstance();
		Answer a = (Answer) answerDB.put( answer );
				
		assertEquals("che tipo di domandone", a.getText() );		
	}
	
	/**
	 * 
	 * Crea e inserisce due risposte ad una domanda,
	 *  verifica che siano inserite correttamente 
	 * 
	 * @throws CustomException
	 */
	@Test(expected = CustomException.class) 
	public void checkAllAnswers() throws CustomException
	{
		Category category = new Category("informatica",null);
		CategoryManager categoryDB = CategoryManager.getInstance();
		Category c = (Category) categoryDB.put( category );

		User user = new User("prova92","prova@prova.com","admin");
		UserManager userDB = new UserManager();
		User u = (User) userDB.put( user );
				
		Question question = new Question("domandone",c,u);
		QuestionManager questionDB = new QuestionManager();
		Question q = (Question) questionDB.put( question );
		
		Answer answer = new Answer("che tipo di domandone",q,u);
		AnswerManager answerDB = AnswerManager.getInstance();
		Answer a = (Answer) answerDB.put( answer );
		Answer answer2 = new Answer("mi piace questo domandone",q,u);
		Answer a2 = (Answer) answerDB.put( answer2 );
		
		assertEquals(answerDB.get(q).size(),2);
		
	}
	
	/**
	 * 
	 * Inserisce un giudizio ad una risposta
	 * e controlla che l'inserimento della question
	 * con giudizio sia corretto
	 * 
	 * @throws CustomException
	 */
	@Test(expected = CustomException.class) 
	public void checkJudgement() throws CustomException
	{
		Category category = new Category("informatica",null);
		CategoryManager categoryDB = CategoryManager.getInstance();
		Category c = (Category) categoryDB.put( category );

		User user = new User("prova92","prova@prova.com","admin");
		UserManager userDB = new UserManager();
		User u = (User) userDB.put( user );
				
		Question question = new Question("domandone",c,u);
		QuestionManager questionDB = new QuestionManager();
		Question q = (Question) questionDB.put( question );
		
		Answer answer = new Answer("che tipo di domandone",q,u);
		AnswerManager answerDB = AnswerManager.getInstance();
		Answer a = (Answer) answerDB.put( answer );
		
		assertNull(a.getAverageVote());
		
		Judgement j = new Judgement(3,u);
		a.addJudgement(j);
		a = (Answer) answerDB.put( a );
		
		assertEquals(Double.doubleToLongBits(a.getAverageVote()),Double.doubleToLongBits(3));

	}
	
}
