package it.unibo.sweng.server;

import java.util.ArrayList;
import java.util.Map;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import it.unibo.sweng.client.services.UserService;
import it.unibo.sweng.server.persistence.UserManager;
import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.User;

/**
 * Classe UserServiceImpl
 * E' un servlet che estende RemoteServiceServlet di GWT e 
 * implementa il servizio UserService
 * 
 * UserServiceImpl contiene i metodi a disposizione del client.
 * 
 * @author Gianmarco Spinaci
 *
 */
public class UserServiceImpl extends RemoteServiceServlet implements UserService {
	
	// Id seriale aggiunto di default
	private static final long serialVersionUID = 557343110785026553L;

	/**
	 * Metodo che implementa il servzio di registrazione di un utente.
	 * se salta un eccezione, genera una custom exception che si occupa della visualizzazione
	 * 
	 * @throws CustomException
	 */
	@Override
	public User register(Map<String, String> data) throws CustomException {
		
		return new UserManager().put(new User(data));
	}

	/**
	 * Metodo che implementa il servzio di login di un utente.
	 * se salta un eccezione, genera una custom exception che si occupa della visualizzazione
	 * 
	 * @throws CustomException
	 */
	@Override
	public User login(String email, String password) throws CustomException {

		return new UserManager().get(email,password);
	}

	/**
	 * Metodo che implementa il servzio di get di tutti gli utenti registrati (non admin o giudici)
	 * se salta un eccezione, genera una custom exception che si occupa della visualizzazione
	 * 
	 * @throws CustomException
	 */
	@Override
	public ArrayList<User> fetchUsers() throws CustomException {
		
		return new UserManager().getRegistredUsers();
	}

	/**
	 * Metodo che implementa il servzio di elezione di un utente a giudice.
	 * se salta un eccezione, genera una custom exception che si occupa della visualizzazione
	 * 
	 * @throws CustomException
	 */
	@Override
	public User electJudge(User user) throws CustomException {
		
		return new UserManager().update(user);
	}

}
