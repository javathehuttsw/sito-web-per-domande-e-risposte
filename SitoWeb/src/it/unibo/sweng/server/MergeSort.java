package it.unibo.sweng.server;

import java.util.ArrayList;

/**
 * Classe MergeSort
 * 
 * Classe di utilità per l'ordinamento efficiente delle Question e delle Answer
 * La classe ha un metodo statico sort che viene invocato passandogli un ArrayList di 
 * comparable 
 * 
 * @author Filippo Boiani
 *
 */
public class MergeSort {
	
	/**
	 * Ordinamento di un'arraylist di comparable
	 * @param input
	 * @return array ordinato
	 */
	static public ArrayList<Comparable> sort(ArrayList<Comparable> input) {
        return mergeSort(input);
    }
	
	/**
	 * Metodo di ordinamento vero e proprio. 
	 * Accetta in inglesso un ArrayList<Comparable> ed invoca il compareTo sugli elementi 
	 * Questo metodo permette di lasciare alle Classi Question e Answer la responsabilitò di come ordinarsi. 
	 * @param whole
	 * @return array ordinato
	 */
	static public ArrayList<Comparable> mergeSort(ArrayList<Comparable> whole) {
        ArrayList<Comparable> left = new ArrayList<Comparable>();
        ArrayList<Comparable> right = new ArrayList<Comparable>();
        int center;
        
        // Se la dimensione della lista é 0 o 1 ritorno la lista
        // Usato come controllo di terminazione per la ricorsione. 
        if (whole.size() == 1 || whole.size() == 0) {    
            return whole;
        } else {
            center = whole.size()/2;
            // Copia la parte sinistra di whole dentro ad una lista di appoggio left
            for (int i=0; i<center; i++) {
                    left.add(whole.get(i));
            }
 
            // Copio la parte destra di whole dentro ad una lista di appoggio right
            for (int i=center; i<whole.size(); i++) {
                    right.add(whole.get(i));
            }
 
            // Chiamo ricorsivamente il mergesort sulle due metà 
            left  = mergeSort(left);
            right = mergeSort(right);
 
            // Faccio il merge dei risultati
            merge(left, right, whole);
        }
        return whole;
    }
	
	/**
	 * Implementa la parte merge del mergesort. 
	 * Viene invocato dal metodo mergeSort
	 * @param left
	 * @param right
	 * @param whole
	 */
	private static void merge(ArrayList<Comparable> left, ArrayList<Comparable> right, ArrayList<Comparable> whole) {
        int leftIndex = 0;
        int rightIndex = 0;
        int wholeIndex = 0;
         
        // As long as neither the left nor the right ArrayList has
        // been used up, keep taking the smaller of left.get(leftIndex)
        // or right.get(rightIndex) and adding it at both.get(bothIndex).
        while (leftIndex < left.size() && rightIndex < right.size()) {
            if ( (left.get(leftIndex).compareTo(right.get(rightIndex))) < 0) {
                whole.set(wholeIndex, left.get(leftIndex));
                leftIndex++;
            } else {
                whole.set(wholeIndex, right.get(rightIndex));
                rightIndex++;
            }
            wholeIndex++;
        }
 
        ArrayList<Comparable> rest;
        int restIndex;
        if (leftIndex >= left.size()) {
            // The left ArrayList has been use up...
            rest = right;
            restIndex = rightIndex;
        } else {
            // The right ArrayList has been used up...
            rest = left;
            restIndex = leftIndex;
        }
 
        // Copy the rest of whichever ArrayList (left or right) was not used up.
        for (int i=restIndex; i<rest.size(); i++) {
            whole.set(wholeIndex, rest.get(i));
            wholeIndex++;
        }
    }
}
