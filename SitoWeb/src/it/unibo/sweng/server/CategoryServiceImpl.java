package it.unibo.sweng.server;

// Utils
import java.util.ArrayList;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import it.unibo.sweng.client.services.CategoryService;
import it.unibo.sweng.server.persistence.CategoryManager;
// Domain 
import it.unibo.sweng.shared.Category;
import it.unibo.sweng.shared.CustomException;

/**
 * Classe CategoryServiceImpl
 * E' un servlet che estende RemoteServiceServlet di GWT e 
 * implementa il servizio CategoryService
 * 
 * Metodi per la gestione delle categorie.
 * 
 * @author Filippo Boiani
 *
 */
public class CategoryServiceImpl extends RemoteServiceServlet implements CategoryService
{
	// Id seriale aggiunto di default
	private static final long serialVersionUID = 6447769074930252100L;

	/**
	 * Prende la lista delle categorie e sottocategorie a partire dall'id
	 * 
	 */
	@Override
	public ArrayList<Category> fetchCategories(int category) throws CustomException {
		
		return new CategoryManager().getAllSubcategories(category);
	}

	/**
	 * Implementa il servizio di inserimento di una categoria
	 * @throws CustomException
	 */
	@Override
	public Category insertCategory(String name, Category father) throws CustomException {
		
		// Istanzio il category manager
		CategoryManager categoryManager = new CategoryManager();
		// Inserisco la categoria
		return categoryManager.put(
				new Category(
						name, 
						categoryManager.getCategoryById(father.getId())
			));
	}

	/**
	 * Implementa il servizio di rinomina di una categoria. 
	 * Restituisce la lista di tutte le categorie.
	 * @throws CustomException
	 */
	@Override
	public ArrayList<Category> renameCategory(String name, Category category) throws CustomException {
		// Rinomina la cateogira
		new CategoryManager().renameCategory(name, category);
		// Chiama il servizio che prende la lista di cateogire 
		return fetchCategories(0);
	}

	/**
	 * Implementa il servizio di get di tutte le categorie in database
	 * @throws CustomException
	 */
	@Override
	public ArrayList<Category> fetchAllCategories() throws CustomException {
		
		return new CategoryManager().getCategoriesList();
	}
}

