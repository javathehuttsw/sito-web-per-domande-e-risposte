package it.unibo.sweng.server.persistence;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import org.mapdb.DB;

import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.User;

/**
 * Classe UserManager 
 * 
 * Gestisce la connessione al database per la classe User.
 * @author Gianmarco Spinaci
 *
 */
public class UserManager {

	private DB db;
	private final String COLLECTION_NAME = "users";
	
	/**
	 * Costruttore della classe. 
	 * Si connette al database tramite persistence. 
	 */
	public UserManager() { this.db = Persistence.getPersistence().getConnection(); }
		
	/**
	 * Aggiunge uno user al database. 
	 * @param user
	 * @return
	 * @throws CustomException
	 */
	public User put(User user) throws CustomException{
		
		try{
			// Prendo la mappa degli utenti
			ConcurrentMap<String, User> map = db.getTreeMap(COLLECTION_NAME);
			
			
			//TODO controllare che l'utente già inserito non esiste
			if(map.get(user.getEmail()) != null)
				throw new CustomException("Errore, email già in uso");

			//user.setType("default");
			map.put(user.getEmail(), user);
			
			// Salvo i cambiamenti
			db.commit();
			
			return user;
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
		
	}

	
	/**
	 * Prendo un utente in base a email e password (login)
	 * @param email
	 * @param pass
	 * @return
	 * @throws CustomException
	 */
	public User get(String email, String pass) throws CustomException{
		
		try{			
			
			User user = new User();
			// Prendo la mappa degli utenti
			ConcurrentMap<String, User> map = db.getTreeMap(COLLECTION_NAME);
			
			// Se la mail non è settata non vado avanti
			if(email != "" ) {
				
				if(map.get(email) != null) {
					User retUser = map.get(email);
					
					// Se la password è sbagliata lancia l'errore
					if(retUser.getPassword().equals(pass)) {
						user = retUser;
					}
					else	throw new CustomException("Errore, password non corretta");
						
				}
				else {
					throw new CustomException("Errore, email non presente nel database");
				}
					
			} else  {
				throw new CustomException("Errore, inserire email");
			}
			
			return user;
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
		
	}
	
	
	/**
	 * Aggiorna i dati dell'utente: nomina di un giudice
	 * @param user
	 * @return
	 * @throws CustomException
	 */
	public User update(User user) throws CustomException {
		
		try
		{
			// Istanza della struttura dati Mapdb
			Map<String, User> map = db.getTreeMap(COLLECTION_NAME);
						
			// L'id è la mail 
			String id = user.getEmail();
			
			// Controllo se la mail è già stata inserita
			if(map.get(id) == null)
				throw new CustomException("Non e stato trovato nessun utente");
			
			// Setto il tipo e inserisco in database
			user.setType("giudice");
			map.put(id, user);
			
			// Salva su file
			db.commit();
			
			return user;
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
	}

	
	/**
	 * Prendo la lista degli utenti registrati (usato per la nomina dei giudici)
	 * @return
	 * @throws CustomException
	 */
	public ArrayList<User> getRegistredUsers() throws CustomException {
		
		ArrayList<User> users = new ArrayList<User>();
		
		try
		{
			// Istanza della struttura dati Mapdb
			Map<String, User> map = db.getTreeMap(COLLECTION_NAME);
			
			// Se il db è vuoto salta l'eccezione
			if(map.size() == 0) throw new CustomException("Non ci sono utenti");
			
			// Itera ogni keys e aggiunge ogni domanda nella lista di ritorno
			Set<String> keys = map.keySet();
			
			for(String key : keys){
				// Prendo gli utenti che non sono judge o admin
				if(!map.get(key).getType().equals("judge") && !map.get(key).getType().equals("admin")) {
					users.add(map.get(key));
				}
			}
			return users;
			
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
	}
	
	
}
