package it.unibo.sweng.server.persistence;

import it.unibo.sweng.shared.Category;
import it.unibo.sweng.shared.CustomException;

import java.util.Set;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentMap;

import org.mapdb.Atomic;
import org.mapdb.DB;

/**
 * Classe CategoryManager
 * Gestisce la connessione al database per le categorie. 
 * 
 * @author Filippo Boiani
 *
 */
public class CategoryManager{
	
	private final String COLLECTION_NAME = "categories";
	
	private final DB mapDb;
	private ConcurrentMap<Integer, Category> database;
	private Category ROOT; 
	
	// Puntatore usato per generare gli id del database
	public Atomic.Integer categoryIdPointer;
	
	// Istanza del category manager
	private static CategoryManager instance;
	
	// Prende l'istanza del CategoryManager
	public static CategoryManager getInstance()
	{
		if(instance == null ) instance = new CategoryManager();
		return instance;
	}
	
	/**
	 * Costruttore della classe.
	 * Si connette al database e prende il puntatore all'ultimo id
	 */
	public CategoryManager()
	{
		this.mapDb = Persistence.getPersistence().getConnection();
		
		this.categoryIdPointer = this.mapDb.getAtomicInteger( "categoryId" );
		
		database = mapDb.getTreeMap(COLLECTION_NAME);
	}
	
	/**
	 * Metodo di inizializzaione
	 * Aggiunge delle classi di default se il database è vuoto
	 * 
	 */
    public void init(){
    	
    	// Pulisco il Database ?
    	//database.clear();
    	
    	// Creo the Root
    	int id = categoryIdPointer.getAndIncrement();
        ROOT = new Category("Root", null);
        ROOT.setId(id);
                
        try {
        	
        	// Aggiungo la root al database
        	System.out.println("Aggiungo la root");
            database.put(id, ROOT);
            
            // Creo le categorie di default
        	Category environment = new Category("Ambiente", null);
            Category animals = new Category("Animali", null);
            Category art = new Category("Arte e Cultura", null);
            Category tech = new Category("Elettronica e Tecnologia", null);
            Category sport = new Category("Sport", null);
            Category chill = new Category("Svago", null);
            
            // Aggiungo le categorie di default
            System.out.println("Aggiungo le categorie di default");
        	this.put(environment);
            this.put(animals);
            this.put(art);
            this.put(tech);
            this.put(sport);
            this.put(chill);
            
            // Salvo
            mapDb.commit();
            
        } catch( CustomException error ) {
        	System.out.println("Errore in fase di inizializzazione: "+error.getMessage() );
        }
        
    }
    
	/**
	 * Resituisce la lista di categorie. 
	 * @return
	 */
    public ArrayList<Category> getCategoriesList(){
        
    	Set<Integer> elementKeys = database.keySet();
    	
        ArrayList<Category> list = new ArrayList<Category>();
        
        for(int key: elementKeys){
            list.add(database.get(key));
        }
        return list;
    }
    
    /**
     * Ritorna la lista di sottocateogie di una categoria data
     * @param cat
     * @return
     */
 	public ArrayList<Category> getAllSubcategories(Category cat){
     	
 		if( cat == null)
 			return this.getAllSubcategories(0);
 		else 
 			return this.getAllSubcategories(cat.getId());
 		
 	}
    
    /**
     * Restituisce la lista di sottocategorie dato l'id di una categoria
     * @param catId
     * @return
     */
    public ArrayList<Category> getAllSubcategories(int catId){
        
    	ArrayList<Category> list = new ArrayList<Category>();
    	
    	Category father = database.get(catId);
        list.add(father);
        
        // Prendo la lista degli id
        Set<Integer> elementKeys = database.keySet();
        
        for(int key: elementKeys){
        	
        	Category desc = database.get(key);
        	// Se la categoria è una discendente la aggiungo
            if ( isDescendant(desc, father) ) 
            	list.add(desc);
            
        }
        return list;
    }
    
    
    /**
     * Verifica che la cateogira sia discendente di un'altra. 
     * @param subCat
     * @param superCat
     * @return
     */
    private boolean isDescendant(Category subCat, Category superCat){
        
    	// Finchè la sottocateogria ha un padre controlla
    	while( subCat.getFather() != null ){
    		if( subCat.getFather().getId() == superCat.getId() ){
    			
    			// lazy restore: cambio i nomi delle categorie padre il cui nome è stato rinominato
    			if( !subCat.getFather().getName().equals(superCat.getName()) ){
    				subCat.getFather().setName(superCat.getName());
    				database.put(subCat.getId(), subCat);
    				mapDb.commit();
    			}
    			return true;
    		} else {
    			subCat = subCat.getFather();
    		}
    	}
    	return false; 
    }
    
    
    /**
     * restituisce una categoria in base all'id. 
     * @param id
     * @return
     */
    public Category getCategoryById(int id){
    	
        return database.get(id);
    }
    
    /**
     * Inserisce una categoria in database
     * @param category
     * @return
     * @throws CustomException
     */
    public Category put(Category category) throws CustomException {
    	
    	// Genero id
    	try {
    		
    		// Verifico la categoria
    		if( checkCategoryName(category.getName()) ){
    			// Se il padre è nullo lo setto a ROOT
    			if( category.getFather() == null ) 
    				category.setFather(ROOT);
    			
    			// Setto id
        		int pointerId = categoryIdPointer.getAndIncrement();
        		category.setId( pointerId );
                
                // Aggiungo la categoria e salvo
                database.put(pointerId, category);
                mapDb.commit();
            }else{
            	throw new CustomException( "Non ci possono essere due categorie con lo stesso nome" );
            }
    		
    		return category;
            
    	}catch( Exception ex ){
    		throw new CustomException( ex.getMessage() );
    	}
    }
    
   
    /**
     * Controlla che il nome della categoria non sia duplicato
     * @param catName
     * @return
     */
    private boolean checkCategoryName( String catName )
    {
    	Set<Integer> elementKeys = database.keySet();
        
        for(int key: elementKeys){
            if ( database.get(key).getName().equals(catName) ) 
            	return false;
        }
        return true;
    }

    
    /**
     * Rinomina una categoria
     * @param newName
     * @param currentCategory
     * @return
     * @throws CustomException
     */
    public Category renameCategory(String newName, Category currentCategory) throws CustomException 
    {	
    	// Prendo la categoria da rinominare
    	Category realCurrentCategory = database.get(currentCategory.getId());

    	if(currentCategory.getName().equals("Root")) throw new CustomException( "Non puoi modifcare la ROOT" );
    	
    	// Genero id
    	try {
    		// Controllo che non ci siano duplicati 
    		if ( !checkCategoryName(newName) ) throw new CustomException( "Categoria con lo stesso nome!" );
    		
    		// Setto il nuovo nome
    		realCurrentCategory.setName(newName);
    		
        	// Aggiungo la categoria rinominata e salvo
    		database.put( realCurrentCategory.getId(), realCurrentCategory );
            mapDb.commit();
            
            return realCurrentCategory;
            
    	}catch( Exception ex ){
    		throw new CustomException( ex.getMessage() );
    	}
    }
}
