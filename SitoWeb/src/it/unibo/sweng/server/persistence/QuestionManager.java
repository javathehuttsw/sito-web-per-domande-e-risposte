package it.unibo.sweng.server.persistence;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import org.mapdb.DB;

import it.unibo.sweng.shared.Category;
import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.Question;

/**
 * Classe QuestionManager
 * Gestione persistenza delle domande
 * 
 * @author Gianmarco Spinaci
 */

public class QuestionManager{

	private DB db;
	private final String COLLECTION_NAME = "questions";
	
	/**
	 * Costruttore della classe. 
	 * Si connette al database. 
	 */
	public QuestionManager() { this.db = Persistence.getPersistence().getConnection(); }
	
	/**
	 * Inserisce una domanda in database
	 * @param question
	 * @return
	 * @throws CustomException
	 */
	public Question put(Question question) throws CustomException{
		
		try
		{
			// Istanza della struttura dati Mapdb
			ConcurrentMap<Integer, Question> map = db.getTreeMap(COLLECTION_NAME);
						
			// Id di una domanda è hashcode del testo
			int id = question.getText().hashCode();
			
			// Controllo se la domanda è già stata inserita
			if(map.get(id) != null){
				System.out.println("Una domanda con lo stesso testo è stata già fatta!");
				throw new CustomException("Una domanda con lo stesso testo è stata già fatta!");
			}
			
			// Setto la data ggiungo e committo
			question.setDate();
			map.put(id, question);
			db.commit();
			
			return question;
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
		
	}

	
	/**
	 * Restituisce tutte le domande
	 * @return
	 * @throws CustomException
	 */
	public ArrayList<Question> getAll() throws CustomException {

		ArrayList<Question> questions = new ArrayList<Question>();
		
		try
		{
			//istanza della struttura dati Mapdb
			ConcurrentMap<Integer, Question> map = db.getTreeMap(COLLECTION_NAME);
			
			//se il db è vuoto salta l'eccezione
			if(map.size() == 0)
				throw new CustomException("Non ci sono domande");
			
			//itera ogni keys e aggiunge ogni domanda nella lista di ritorno
			Set<Integer> keys = map.keySet();
			for(int key : keys)
				questions.add(map.get(key));
			
			return questions;
			
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
		
	}
	
	
	/**
	 * Restituisco tutte le domande di una categoria e delle sue sottocategorie
	 * @param catList
	 * @return
	 * @throws CustomException
	 */
	public ArrayList<Question> get(ArrayList<Category> catList) throws CustomException {
		
		ArrayList<Question> questions = new ArrayList<Question>();
		
		try
		{
			// Istanza della struttura dati Mapdb
			ConcurrentMap<Integer, Question> map = db.getTreeMap(COLLECTION_NAME);
			
			// Se il db è vuoto salta l'eccezione
			if(map.size() == 0) throw new CustomException("Non ci sono domande");
			
			// Se non c'è una lista di categorie prendo tutte le domande
			Set<Integer> keys = map.keySet();
			
			// Se la lista di categorie è nulla
			if(catList == null ){
				
				// Prendo tutte le domande
				for(int key : keys) questions.add( map.get(key) );
			
			}else {
				
				for(int key : keys) {
					Question q = map.get(key);
					for( Category c: catList ){
						// se la categoria della domanda è uan di qeulle della lista di categorie/sottocategorie
						if( q.getCategory().getId() == c.getId() ){
							
							// lazy replace: se le domande hanno lo stesso id della cat ma il nome è divero lo cambio.
							if( !q.getCategory().getName().equals(c.getName()) ){
								q.getCategory().setName(c.getName());
								map.put(key, q);
								db.commit();
							}
							
							questions.add( q );
						}
					}
				}
			}
			
			if(questions.size()==0) throw new CustomException("Non ci sono domande per questa categoria!");
			
			return questions;
			
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
	}
	

	/**
	 * Canello una domanda. 
	 * @param question
	 * @return
	 * @throws CustomException
	 */
	public String delete(Question question) throws CustomException{
		
		
		try
		{
			//istanza della struttura dati Mapdb
			Map<Integer, Question> map = db.getTreeMap(COLLECTION_NAME);
						
			//id di una domanda è hashcode del testo
			int id = question.getText().hashCode();
			
			//controllo se la domanda è prensente e cancello
			if(map.get(id) != null) {
				// Rimuovo e committo
				map.remove(id);
				db.commit();
			}else {
				throw new CustomException("La domanda non sembra essere presente nel DB");

			}
			
			System.out.println("Domanda eliminata con successo");
			return "Domanda eliminata con successo";
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
		
	}
}
