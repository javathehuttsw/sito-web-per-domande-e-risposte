package it.unibo.sweng.server.persistence;

// Domain elements 
import it.unibo.sweng.shared.Answer;
import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.Question;
// Utils
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
// MapDB
import org.mapdb.DB;

/**
 * Classe AnswerManager
 * 
 * Gestisce la connessione al databse per la classe Answer
 * @author Filippo Boiani
 *
 */
public class AnswerManager {
	
	private DB db;
	// Nome della collezione 
	private final String COLLECTION_NAME = "answers";
	// Istanza della classe AnswerManager
	private static AnswerManager instance;
	
	/**
	 * Costruttore della classe AnswerManager
	 */
	public AnswerManager()
	{
		this.db = Persistence.getPersistence().getConnection();
	}

	/**
	 * Ritorna l'istanza della classe, se non esiste la crea. 
	 * @return
	 */
	public static AnswerManager getInstance()
	{
		if(instance == null ) instance = new AnswerManager();
		
		return instance;
	}
	
	/**
	 * Inserisce la domanda in database.
	 * 
	 * @param answer
	 * @return answer
	 * @throws CustomException
	 */
	public Answer put(Answer answer) throws CustomException {
		
		try
		{
			// Istanza della struttura dati Mapdb
			ConcurrentMap<Integer, Answer> map = db.getTreeMap(COLLECTION_NAME);
						
			// Genero l'id che sarà l'hashcode del testo 
			int id = answer.getText().hashCode();
			
			// Controllo se una risposta con lo stesso testo è già stata data
			if(map.get(id) != null)
				throw new CustomException("Una risposta identica è già stata data!!");
			
			// Setto la data
			answer.setDate();
			// Inserisce in database
			map.put(id, answer);
			// Salva su file
			db.commit();
			
			// Restituisco la risposta
			return answer;
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
	}

	/**
	 * Restituisce tutte le risposte di una data domanda.
	 * @param question
	 * @return
	 * @throws CustomException
	 */
	public ArrayList<Answer> get(Question question) throws CustomException {
		
		// Lista delle answer 
		ArrayList<Answer> answers = new ArrayList<Answer>();
		
		try
		{
			// Istanza della struttura dati Mapdb
			ConcurrentMap<Integer, Answer> map = db.getTreeMap(COLLECTION_NAME);
			
			// Se il db è vuoto salta l'eccezione
			if(map.size() == 0)
				throw new CustomException("Non ci sono risposte");
			
			// Itera ogni keys e aggiunge ogni domanda nella lista di ritorno
			Set<Integer> keys = map.keySet();
			for(int key : keys) {
				Answer ans = map.get(key);
				// Se la domanda corrisponde a quella question
				if (ans.getQuestion().getText().equals(question.getText())) {
					answers.add(ans);
				}
			}	
			
			// Se non ci sono risposte lancio l'eccezione
			if(answers.size() == 0)
				throw new CustomException("Non ci sono ancora risposte per questa domanda");
			
			return answers;
			
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
	}
	
	
	/**
	 * Aggiunge il giudizio ad una risposta
	 * @param model
	 * @return answer
	 * @throws CustomException
	 */
	public Answer update(Answer answer) throws CustomException {

		try
		{
			// Istanza della struttura dati Mapdb
			ConcurrentMap<Integer, Answer> map = db.getTreeMap(COLLECTION_NAME);
						
			// Calcolo l'id della risposta a partire dal testo
			int id = answer.getText().hashCode();
			
			// Controllo se la domanda è preente in database 
			if(map.get(id) == null)
				throw new CustomException("Non è stata rilevata la domanda");
			
			// Aggionro la risposta
			map.put(id, answer);
			// Salva su file 
			db.commit();
			
			return answer;
		}
		catch(Exception ex)
		{
			db.rollback();
			throw new CustomException(ex.getMessage());
		}
	}
	
	
	/**
	 * Cancella tutte le rispote di una domanda
	 * @param question
	 * @throws CustomException
	 */
	public void deleteAnswersByQuestion(Question question) throws CustomException {
		
		try
		{
			// Istanza della struttura dati Mapdb
			Map<Integer, Answer> map = db.getTreeMap(COLLECTION_NAME);
						
			// Prendo l'id
			int id = question.getText().hashCode();
			Set<Integer> keys = map.keySet();
							
			for(int key : keys) {
				if( map.get(key).getQuestion().getText().hashCode() == (id) ) map.remove(key);
			}
			
			//committo i cambiamenti
			db.commit();
	
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
		
	}

	
	/**
	 * Cancella una risposta
	 * @param answer
	 * @return
	 * @throws CustomException
	 */
	public String delete(Answer answer) throws CustomException{
		
		
		try
		{
			// Istanza della struttura dati Mapdb
			Map<Integer, Answer> map = db.getTreeMap(COLLECTION_NAME);
						
			// Id di una domanda è hashcode del testo
			int id = answer.getText().hashCode();
			
			// Controllo se la risposta è presente e cancello
			if(map.get(id) != null) {
				// Rimuovo e committo
				map.remove(id);
				db.commit();
			}else {
				throw new CustomException("La risposta non sembra essere presente nel DB");

			}
			
			return "Risposta eliminata con successo";
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage());
		}
		
	}

}
