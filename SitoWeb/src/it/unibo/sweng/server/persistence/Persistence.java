package it.unibo.sweng.server.persistence;

// Utils
import java.io.File;
import java.util.concurrent.Semaphore;
// MapDB
import org.mapdb.DB;
import org.mapdb.DBMaker;

/**
 * Classe astratta Persistence, si occupa di mettere a disposizione
 * i metodi alle classi figlio
 * 
 * ha anche un metodo che restituisce il DB chiamato sweng.db
 * 
 * @author Gianmarco Spinaci
 *
 */
public class Persistence {

	//nome del file db
	private final String FILE_NAME = "sweng.db";
	private static Semaphore semaphore = new Semaphore(1);
	public DB db; 
	
	// Costruttore di default 
	private Persistence() {}
	
	// Istanza della classe disponibile tramite getPersistence()
	private static Persistence persistence;
	
	/**
	 * Restituisce l'istanza della classe, se non esiste la crea. 
	 * In questo modo implementa il pattern singleton.
	 * 
	 * @return persistence
	 */
	public static Persistence getPersistence()
	{
		if(persistence == null) persistence = new Persistence();
		
		return persistence;
	}
	
	//restituisce la connessione al db
	public DB getConnection() {
		return getDB();
	}
	
	/**
	 * Prende l'istanza del database, anche il file sarà un singleton. 
	 * 
	 * @return db
	 */
	private DB getDB() {
		
		try {
			// Acquisisco il lock
	        semaphore.acquire();
	        
			if(db == null) {
				db = DBMaker.newFileDB(new File(FILE_NAME)).closeOnJvmShutdown().make();
			}
			return db;
			
	    } catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			// Rilascio il lock
	        semaphore.release();
	    }
		return db;
	}	
}