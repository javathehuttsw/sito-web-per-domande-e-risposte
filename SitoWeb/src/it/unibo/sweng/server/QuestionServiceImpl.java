package it.unibo.sweng.server;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import it.unibo.sweng.client.services.QuestionService;
import it.unibo.sweng.server.persistence.AnswerManager;
import it.unibo.sweng.server.persistence.CategoryManager;
import it.unibo.sweng.server.persistence.QuestionManager;
import it.unibo.sweng.shared.Answer;
import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.Question;
import it.unibo.sweng.shared.User;

/**
 * Classe QuestionServiceImpl
 * E' un servlet che estende RemoteServiceServlet di GWT e 
 * implementa il servizio QuestionService
 * 
 * QuestionServiceImpl contiene i metodi a disposizione del client.
 * 
 * @author Gianmarco Spinaci
 *
 */
public class QuestionServiceImpl extends RemoteServiceServlet implements QuestionService{

	// id seriale aggiunto di default
	private static final long serialVersionUID = -1581819193760680202L;

	/**
	 * Metodo per inserire una domanda, accettando testo e categoria
	 * se salta un eccezione, genera una custom exception che si occupa della visualizzazione
	 * @throws CustomException
	 */
	@Override
	public String insertQuestion(String text, int idCategory, User user) throws CustomException{
		
		return new QuestionManager().put(new Question(
				text,
				new CategoryManager().getCategoryById(idCategory),
				user
			)).getText();
	}
	
	
	/**
	 * Metodo per ricevere tutte le domande che fanno capo a quella categoria
	 * @throws CustomException
	 */
	@Override
	public ArrayList<Question> fetchQuestions(int idCategory) throws CustomException{
		
		//prende tutte le domande delle sottocategorie della categoria scelta
		ArrayList<Question> questions = new QuestionManager().get(
				new CategoryManager().getAllSubcategories(idCategory)
			);

		ArrayList<Comparable> comp = new ArrayList<>();
		
		//aggiungo ogni question alla lista di comparable
		//TODO usare le lambda queries 
		for(Question question : questions){
			
			comp.add(question);
		}
		
		questions.clear();
		
		//faccio il mergeSort della lista
		for(Comparable str: MergeSort.sort(comp)){
			questions.add((Question)str);
		}
		
		return questions;
	}

	/**
	 * Metodo che implementa il servizio di inseriemnto di una risposta ad una domanda.
	 * Chiama il manager del database per le Answer e ritorno il risultato
	 * 
	 * @throws CustomException
	 */
	@Override
	public Answer insertAnswer(String text, Question question, User user) throws CustomException {
		
		return new AnswerManager().put(new Answer(text,question,user));
	}

	/**
	 * Metodo che implementa il servizio di inseriemnto di una giudizio ad una risposta.
	 * Chiama il manager del database per le Answer e ritorno il risultato
	 * 
	 * @throws CustomException
	 */
	@Override
	public ArrayList<Answer> updateAnswer(Answer answer) throws CustomException {
		
		AnswerManager answerManager = new AnswerManager();
		
		//update delle risposte
		answerManager.update(answer);
		
		//ritorno la nuova lista delle risposte
		return this.fetchAnswers(answer.getQuestion());
	}
	
	/**
	 * Metodo che implementa il servizio di get di tutte le risposte ad una domanda.
	 * Chiama il manager del database per le Answer e ritorno il risultato
	 * 
	 * @throws CustomException
	 */
	@Override
	public ArrayList<Answer> fetchAnswers(Question question) throws CustomException {
		
		// Prendo la lista di answer
		ArrayList<Answer> answers = new AnswerManager().get(question);

		ArrayList<Comparable> comp = new ArrayList<>();
		
		//TODO possibilità di usare le lambda queries 
		for(Answer answer : answers){
			
			comp.add(answer);
		}
		
		answers.clear();
		
		//faccio il mergeSort della lista
		for(Comparable str: MergeSort.sort(comp)){
			answers.add((Answer)str);
		}
		
		return answers;
	}

	/**
	 * Metodo che implementa il servizio di cancellazione di una Domanda.
	 * Chiama il manager del database per le Answer per cancellare le risposte e poi cancello la domanda.
	 * 
	 * @throws CustomException
	 */
	@Override
	public String deleteQuestion(Question question) throws CustomException {
		
		//elimino tutte le risposte della domanda
		new AnswerManager().deleteAnswersByQuestion(question);
		
		//elimino la domanda
		return new QuestionManager().delete(question);
	}

	/**
	 * Metodo che implementa il servizio di cancellazione di una risposta.
	 * Chiama il manager del database per le Answer e ritorno il risultato
	 * 
	 * @throws CustomException
	 */
	@Override
	public String deleteAnswer(Answer answer) throws CustomException {
		
		return new AnswerManager().delete(answer);
	}

}
