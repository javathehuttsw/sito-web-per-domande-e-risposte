package it.unibo.sweng.server;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import it.unibo.sweng.client.services.SitoWebService;
import it.unibo.sweng.server.persistence.CategoryManager;
import it.unibo.sweng.server.persistence.UserManager;
import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.User;
/**
 * 
 * Classe SitoWebServiceImpl
 * E' un servlet che estende RemoteServiceServlet di GWT e 
 * implementa il servizio SitoWebService
 * 
 * SitoWebServiceImpl contiene i metodi utilizzati per inizializzare il database. 
 * 
 * @author Gianmarco Spinaci
 *
 */
public class SitoWebServiceImpl extends RemoteServiceServlet implements SitoWebService{

	// id seriale aggiunto di default
	private static final long serialVersionUID = 8382103644321418586L;

	@Override
	public boolean setup() throws CustomException{
		
		// metodo che crea l'utente root
		Map<String, String> data = new HashMap<String, String>();
		
		data.put("username", "admin");
		data.put("password", "admin");
		data.put("email", "admin@admin.com");
		data.put("type", "admin");
		
		new UserManager().put(new User(data));
		
		// Genera le categorie di default
		new CategoryManager().init();
		
		return true;
	}


}
