package it.unibo.sweng.shared;

import java.io.Serializable;

/**
 * Classe CategoryDuplicationException
 * 
 * Utilizzata per mandare un erroe in caso di cateogie duplicate. 
 * @author Filippo Boiani
 *
 */
public class CategoryDuplicationException extends Exception implements Serializable{
	
	// id aggiunto di default
	private static final long serialVersionUID = -3376368168651197554L;
	
	// Messaggio di errore
	String message = "La categoria specificata è già esistente.";
	
	// Costruttore di default per la serializzazione
	public CategoryDuplicationException() {}

}
