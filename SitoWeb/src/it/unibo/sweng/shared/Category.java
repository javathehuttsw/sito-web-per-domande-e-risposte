package it.unibo.sweng.shared;

import java.io.Serializable;

/**
 * Classe Category
 * 
 * Rappresenta le categorie a cui appartengono le domande. 
 * Ogni categoria ha un nome modificabile, un id per il database e un riferimento 
 * alla cateogria padre. 
 * 
 * @author Filippo Boiani
 *
 */
public class Category implements Serializable 
{
	// Id seriale aggiunto di default 
	private static final long serialVersionUID = 3382607457060741410L;
	private int id;
    private String name; 
    
    // Sovra-categoria
    private Category father; 
    
    // Costruttore di default per la serailizzazione 
    public Category(){}
    
    /**
     * Costruttore della classe Cateogry
     * 
     * @param name
     * @param father
     */
    public Category(String name, Category father){
        this.name = name; 
        this.father = father;
    }
    
    /**
     * Restituisce il nome della cateogria. 
     * @return name
     */
    public String getName() {
        return name;
    }
    
    /**
     * Restituisce l'id del database. 
     * @return id
     */
    public int getId() {
        return id;
    }
    
    /**
     * Ritorna il riferimento alla categoria genitore. 
     * @return father
     */
    public Category getFather() {
        return father;
    }
    
    /**
     * Setta il nome della categoria. 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * Setta l'id della categoria. 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * Setta il genitore della categoria. 
     * @param father
     */
    public void setFather(Category father) {
        this.father = father;
    }
    
    /**
     * Override del metodo toString
     * @return
     */
    public String toSring(){
        return "{ cat_id: '"+this.id+"' , cat_name: '"+this.name+"' }"; 
    }

}
