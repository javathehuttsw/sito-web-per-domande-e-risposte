package it.unibo.sweng.shared;

import java.io.Serializable;
import java.util.Date;

public class Judgement implements Serializable
{
	// Id seriale di default
	private static final long serialVersionUID = 8700992869873849008L;
	
	private int vote;
	private User judge;
	private Date date;
	
	// Costruttore di default aggiunto per la serializzazione
	public Judgement () {}
	
	/**
	 * Costruttore delle classe Judgement 
	 * 
	 * @param vote
	 * @param judge
	 */
	public Judgement(int vote, User judge) {
		super();
		this.vote = vote;
		this.judge = judge;
		setDate();
	}
 
	/**
	 * Setta la data a quella attuale 
	 */
	public void setDate() {
		this.date = new Date();
	}
	
	/**
	 * Restituisce la data 
	 * @return date 
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * Restituisce il voto 
	 * @return vote
	 */
	public int getVote() {
		return vote;
	}
	
	/**
	 * Restituisce l'utente giudice che ha dato il voto. 
	 * @return judge
	 */
	public User getJudge() {
		return judge;
	}
	
	/**
	 * Override del metodo toString 
	 */
	@Override
	public String toString() {
		return "Judgement [vote=" + vote + ", judge=" + judge + "]";
	} 
}