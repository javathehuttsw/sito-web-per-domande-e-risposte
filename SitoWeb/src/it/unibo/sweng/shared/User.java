package it.unibo.sweng.shared;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * User è la classe che identifica l'utente
 * i primi 3 parametri sono obbligatori
 * i restanti sono opzionali (se non ci sono sono stringa vuota)
 * 
 * Il campo tipologia è di default "registered"
 * 
 * @author Gianmarco Spinaci
 */
public class User implements Serializable{
	
	// Id aggiunto di default 
	private static final long serialVersionUID = 3390510657573902340L;
	
	private String username;
	private String password;
	private String email;
	
	//Parametri opzionali
	private String name;
	private String lastName;
	private String gender;
	private String bornDate;
	private String bornPlace;
	private String home;
	
	//tipo registrato, giudice, admin
	private String type;
	
	// Costruttore di default per la serializzazione 
	public User() {}
	
	/**
	 * Costruttore della classe User
	 * @param username
	 * @param email
	 * @param password
	 */
	public User(String username, String email, String password) {
		this.username = username;
		this.email = email; 
		this.password = password; 
	}
	
	/**
	 * Costruttore della classe User, accetta una mappa con i parametri di registrazione
	 * @param data
	 */
	public User(Map<String, String> data){
		
		setUsername(data.get("username"));
		setPassword(data.get("password"));
		setEmail(data.get("email"));
		
		setName(data.get("name"));
		setLastName(data.get("lastName"));
		setGender(data.get("gender"));
		setBornDate(data.get("bornDate"));
		setBornPlace(data.get("bornPlace"));
		setHome(data.get("home"));

		setType( data.get("type") == null ? "registrato" : data.get("type") );
	}

	/**
	 * getter dello username
	 * @return username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Setter dello username
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * Getter della password
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Setter della password
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Getter della mail
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Setter della mail
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Getter del nome
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter del nome
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Getter del cognome
	 * @return lastName
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Setter del cognome
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Getter del sesso
	 * @return
	 */
	public String getGender() {
		return gender;
	}
	
	/**
	 * Setter del sesso
	 * @param gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * getter della data di nascita
	 * @return
	 */
	public String getBornDate() {
		return bornDate;
	}
	
	/**
	 * Setter della data di nascita
	 * @param bornDate
	 */
	public void setBornDate(String bornDate) {
		this.bornDate = bornDate;
	}
	
	/**
	 * Getter del luogo di nascita
	 * @return
	 */
	public String getBornPlace() {
		return bornPlace;
	}
	
	/**
	 * Setter del luogo di nascita
	 * @param bornPlace
	 */
	public void setBornPlace(String bornPlace) {
		this.bornPlace = bornPlace;
	}
	
	/**
	 * Getter della residenza
	 * @return
	 */
	public String getHome() {
		return home;
	}
	
	/**
	 * Setter della residenza
	 * @param home
	 */
	public void setHome(String home) {
		this.home = home;
	}
	
	/**
	 * Getter del tipo di utente
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Setter del tipo di utente
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * L'utente è admin?
	 * @return
	 */
	public boolean isAdmin(){
		return this.type.equals("admin");
	}
	
	/**
	 * L'utente è giudice?
	 * @return
	 */
	public boolean isJudge(){
		return this.type.equals("giudice");
	}


}
