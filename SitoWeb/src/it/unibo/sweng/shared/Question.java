package it.unibo.sweng.shared;

import java.io.Serializable;
import java.util.Date;

/**
 * Question è la classe che identifica una domanda, è serializzabile
 * ed è un Comparable
 * 
 * è definita da un testo, categoria, data e utente creatore
 * 
 * @author Gianmarco Spinaci
 *
 */
public class Question implements Serializable, Comparable<Object>  {

	// Id seriale aggiunto di default 
	private static final long serialVersionUID = -2647826878375206500L;
 
	private String text;
	private Category category;
	private Date date;
	private User creator; 
	
	// Costruttore di default per la serializzazione
	public Question() {}
	
	/**
	 * Costruttore della classe Question 
	 * 
	 * @param text
	 * @param category
	 * @param creator
	 */
	public Question(String text, Category category,  User creator)
	{
		super();
		this.text = text;
		this.category = category;
		this.creator = creator; 
	}
	
	/**
	 * Restituisce il testo della domanda
	 * @return text 
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Setta il testo della domanda
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * Restituisce la cateogria a cui appartiene la domanda.
	 * @return category
	 */
	public Category getCategory() {
		return category;
	}
	
	/**
	 * Setta la categoria della domanda
	 * @param category
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
	
	/**
	 * Restituisce l'utente che ha scritto la domanda 
	 * @return creator
	 */
	public User getCreator() {
		return creator;
	}
	
	/**
	 * Setta il creatore della domanda 
	 * @param creator
	 */
	public void setCreator(User creator) {
		this.creator = creator;
	}
	
	/**
	 * Restituisce la data in cui stata effettuata la domanda
	 * @return date
	 */
	public Date getDate() {
		return date;
	}
 
	/**
	 * Setta la data della domanda
	 */
	public void setDate() {
		this.date = new Date();
	}
	
	/**
	 * Override del metodo toString
	 */
	public String toString(){
		return this.text;
	}
	
	/**
	 * Override del metodo compareTo 
	 */
	@Override
	public int compareTo(Object o){
		return compareTo((Question) o); 
	}
	
	/**
	 * Ordinamento delle domande in base alla data. 
	 * @param question
	 * @return
	 */
	public int compareTo(Question question) {
		return question.getDate().compareTo(this.getDate());
	}
}