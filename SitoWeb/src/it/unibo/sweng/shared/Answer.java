package it.unibo.sweng.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Question è¨ la classe che identifica una domanda, è serializzabile
 * ed è un Comparable
 * 
 * è definita da un testo e una categoria (categoria è una stringa)
 * 
 * @author Gianmarco Spinaci
 * 
 */
public class Answer implements Serializable, Comparable<Object>  {
	
	// Id aggiunto di default 
	private static final long serialVersionUID = -2647826878375206500L;
 
	private String text;
	private Question question;
	private Date date;
	// La lista dei voti dati alla domanda
	private ArrayList<Judgement> voteList;
	private User creator; 
	
	// Costruttore di default per la serializzazione
	public Answer() {}
 
	/**
	 * Costruttore della classe Answer
	 * @param text
	 * @param question
	 * @param creator
	 */
	public Answer(String text, Question question, User creator) {
		this.text = text;
		this.question = question;
		this.creator = creator; 
		this.setVoteList(new ArrayList<Judgement>());
	}
	
	/**
	 * Getter del testo della risposta 
	 * @return text
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Setter del testo della risposta
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * Getter della domanda a cui la risposta è riferita
	 * @return question
	 */
	public Question getQuestion() {
		return question;
	}
	
	/**
	 * Setter della domanda a cui la risposta si riferisce
	 * @param question
	 */
	public void setQuestion(Question question) {
		this.question = question;
	}

	/**
	 * Setter della data di risposta
	 */
	public void setDate() {
		this.date = new Date();
	}

	/**
	 * Getter della data di risposta
	 * @return
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * Getter dell'utente creatore
	 * @return
	 */
	public User getCreator() {
		return creator;
	}
	
	/**
	 * Setter dell'utente creatore
	 * @param creator
	 */
	public void setCreator(User creator) {
		this.creator = creator;
	}

	/**
	 * Override del motodo toString
	 */
	public String toString(){
		return this.text;
	}
	
	/**
	 * Aggiunge un giudizio alla risposta
	 * @param jdgmt
	 */
	public void addJudgement(Judgement jdgmt){
		this.voteList.add(jdgmt);
	}
 
	/**
	 * Calcola il voto medio dalla lista dei voti e le restituisce.
	 * @return
	 */
	public double getAverageVote(){
		double avg = 0;
		// Se la domanda ha voti 
		if( hasJudgements() ){
			// Per ogni giudizio prendo il voto e lo sommo 
			for(Judgement j: voteList) {
				avg += j.getVote();
			}
			return  avg/voteList.size();
	  
		}else {
			return -1;
		}
  
	}
 
	/**
	 * Restituisce la lista dei voti
	 * @return voteList
	 */
	public ArrayList<Judgement> getVoteList() {
		return voteList;
	}

	/**
	 * Setta la lista dei voti 
	 * @param voteList
	 */
	public void setVoteList(ArrayList<Judgement> voteList) {
		this.voteList = voteList;
	}

	/**
	 * Override del metodo compareTo
	 */
	@Override
	public int compareTo(Object o){
		return compareTo((Answer) o); 
	}
  
	/**
	 * Le answer vengono ordinate in base al voto, 
	 * Le answer con voto hanno priorità su quelle senza giudizi. 
	 * In caso di giudizio uguale ha la priorità quella con la data più recente. 
	 * 
	 * Le risposte senza voti sono in ordine di data. 
	 * 
	 * @param answer
	 * @return int
	 */
	public int compareTo(Answer answer) {
		//if d1 > d2 return +1
		if( Double.compare( answer.getAverageVote(), this.getAverageVote() ) ==  0 ) {
			return answer.getDate().compareTo(this.getDate());
    
		}else {
			return Double.compare( answer.getAverageVote(), this.getAverageVote() );
		}
	}
  
	/**
	 * La risposta ha avuto dei giudizi?
	 * @return
	 */
	public boolean hasJudgements(){
		return voteList.size() != 0; 
	}
  
	/**
	 * Metodo hadJudge. 
	 * Dato un utente restituisce true se questo ha già giudicato la risposta.
	 * @param u
	 * @return boolean
	 */
	public boolean hasJudge(User u){
		if( u != null ) {
			for(Judgement j : voteList) {
				if( j.getJudge().getEmail().equals( u.getEmail() ) ) {return true;}
			}
		}
		return false;
	}
 
}