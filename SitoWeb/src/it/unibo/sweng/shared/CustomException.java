package it.unibo.sweng.shared;

import java.io.Serializable;

/**
 * Classe CustomException. 
 * 
 * Questa classe è utilizzta per restituire errori al client nel caso di input scorretti 
 * o errori nel server. 
 * 
 * @author Gianmarco Spinaci
 *
 */
public class CustomException extends Exception implements Serializable{
	
	// Id seriale aggiunto di default
	private static final long serialVersionUID = -8524471533851153386L;
	
	// Costruttore di default per la serailizzazione
	public CustomException() { }
	
	/**
	 * Costruttore della classe eccezione. 
	 * @param message
	 */
	public CustomException(String message) {
		super(message);
	}
}
