package it.unibo.sweng.client.services;

import java.util.ArrayList;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.User;

/**
 * Interfaccia UserService
 * 
 * Servizi messi a disposizione del client per la gestione dell'utente
 * @extends RemoteService
 * @author Gianmarco Spinaci
 *
 */
@RemoteServiceRelativePath("user")
public interface UserService extends RemoteService {
	
	// Registrazione
	User register(Map<String, String> data) throws CustomException;
	
	// Login
	User login(String email,String password) throws CustomException;
	
	// Lista degli utenti 
	ArrayList<User> fetchUsers() throws CustomException;

	// Elezione di un giudice
	User electJudge(User u) throws CustomException;
}
