package it.unibo.sweng.client.services;

import java.util.ArrayList;
import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;

import it.unibo.sweng.shared.User;

/**
 * UserServiceAsync
 * Versione asincrona del UserService
 * 
 * @author Gianmarco Spinaci
 *
 */
public interface UserServiceAsync {

	void register(Map<String, String> data, AsyncCallback<User> callback);

	void login(String email, String password, AsyncCallback<User> callback);

	void fetchUsers(AsyncCallback<ArrayList<User>> asyncCallback);

	void electJudge(User u, AsyncCallback<User> asyncCallback);
}
