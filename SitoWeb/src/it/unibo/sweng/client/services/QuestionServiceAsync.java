package it.unibo.sweng.client.services;

import it.unibo.sweng.shared.Answer;
import it.unibo.sweng.shared.Question;
import it.unibo.sweng.shared.User;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * QuestionServiceAsync
 * Versione asincrona del QuestionService
 * 
 * @author Gianmarco Spinaci
 *
 */
public interface QuestionServiceAsync {

	//metodi insert
	void insertQuestion(String text, int category, User user, AsyncCallback<String> callback);
	
	void insertAnswer(String text, Question question, User user, AsyncCallback<Answer> asyncCallback);

	//metodi fetch
	void fetchQuestions(int idCategory, AsyncCallback<ArrayList<Question>> callback);

	void fetchAnswers(Question question,AsyncCallback<ArrayList<Answer>> asyncCallback);
	
	//metodo update
	void updateAnswer(Answer a, AsyncCallback<ArrayList<Answer>> asyncCallback);

	//metodi delete
	void deleteQuestion(Question question, AsyncCallback<String> asyncCallback);

	void deleteAnswer(Answer answer, AsyncCallback<String> asyncCallback);

}
