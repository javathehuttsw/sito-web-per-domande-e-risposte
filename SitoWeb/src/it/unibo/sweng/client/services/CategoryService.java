package it.unibo.sweng.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import it.unibo.sweng.shared.Category;
import it.unibo.sweng.shared.CustomException;

/**
 * Interfaccia CategoryService
 * 
 * Servizi messi a disposizione del client per la gestione delle categorie
 * @extends RemoteService
 * @author Filippo Boiani
 *
 */
@RemoteServiceRelativePath("category")
public interface CategoryService extends RemoteService {
	
	// Inserisce una categoria
	Category insertCategory(String newCategoryName, Category father) throws CustomException; 
	
	// Prende la lista di categoire e sottocategoira di un id specificato
	ArrayList<Category> fetchCategories(int category) throws CustomException;
	
	// Rinomina una categoria
	ArrayList<Category> renameCategory(String name, Category category) throws CustomException;
	
	// Prende la lista di tutte le categorie
	ArrayList<Category> fetchAllCategories() throws CustomException;
}