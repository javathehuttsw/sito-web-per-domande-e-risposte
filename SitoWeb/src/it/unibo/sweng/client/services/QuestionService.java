package it.unibo.sweng.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import it.unibo.sweng.shared.Answer;
import it.unibo.sweng.shared.CustomException;
import it.unibo.sweng.shared.Question;
import it.unibo.sweng.shared.User;
/**
 * Interfaccia QuestionService
 * 
 * Servizi messi a disposizione del client per la gestione delle domande e delle risposte
 * @extends RemoteService
 * @author Filippo Boiani
 *
 */
@RemoteServiceRelativePath("question")
public interface QuestionService extends RemoteService {

	// Inserire una domanda
	String insertQuestion(String text, int category, User user) throws CustomException; 
	
	// Inserire una risposta
	Answer insertAnswer(String text, Question question, User user) throws CustomException;
	
	// Prendere le domande di quella categoria (e delle sottocategorie)
	ArrayList<Question> fetchQuestions(int idCategory) throws CustomException;
	
	// Prendere le lista di risposte ad una domanda
	ArrayList<Answer> fetchAnswers(Question question) throws CustomException; 
	
	// Aggiunge un giudizio alla risposta
	ArrayList<Answer> updateAnswer(Answer a) throws CustomException;
	
	// Cancella una domanda
	String deleteQuestion(Question question) throws CustomException;
	
	// Cancella una risposta
	String deleteAnswer(Answer answer) throws CustomException;

}
