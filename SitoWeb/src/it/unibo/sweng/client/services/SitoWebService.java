package it.unibo.sweng.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import it.unibo.sweng.shared.CustomException;
/**
 * Interfaccia SitoWebService
 * 
 * Servizi messi a disposizione del client per l'inizializzazione del database
 * @extends RemoteService
 * @author Gianmarco Spinaci
 *
 */
@RemoteServiceRelativePath("sitoWeb")
public interface SitoWebService extends RemoteService {
	
	// Set up del database
	boolean setup() throws CustomException;
}