package it.unibo.sweng.client.services;

import it.unibo.sweng.shared.Category;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * CategoryServiceAsync
 * Versione asincrona del CategoryService
 * 
 * @author Filippo Boiani
 *
 */
public interface CategoryServiceAsync {
	
	void insertCategory(String newCategoryName, Category father, AsyncCallback<Category> callback);

	void fetchCategories(int category, AsyncCallback<ArrayList<Category>> callback);

	void renameCategory(String name, Category category, AsyncCallback<ArrayList<Category>> callback);

	void fetchAllCategories(AsyncCallback<ArrayList<Category>> callback);

}
