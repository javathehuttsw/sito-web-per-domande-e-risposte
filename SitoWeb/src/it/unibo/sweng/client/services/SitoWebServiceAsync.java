package it.unibo.sweng.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * SitoWebServiceAsync
 * Versione asincrona del SitoWebService
 * 
 * @author Gianmarco Spinaci
 *
 */
public interface SitoWebServiceAsync {

	void setup(AsyncCallback<Boolean> callback);

}
