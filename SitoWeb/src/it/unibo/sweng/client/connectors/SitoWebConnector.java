package it.unibo.sweng.client.connectors;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import it.unibo.sweng.client.services.SitoWebService;
import it.unibo.sweng.client.services.SitoWebServiceAsync;

/**
 * 
 * SitoWebConnector
 * 
 * Gestisce la connessione al servizio di setup del database
 * @author Gianmarco Spinaci
 *
 */
public class SitoWebConnector {

	private SitoWebServiceAsync sitoWebService;
	
	public void initServer()
	{
		if (sitoWebService == null) sitoWebService = GWT.create(SitoWebService.class); 
		
		//richiama il server per il setup 
		sitoWebService.setup(new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				// non faccio nulla
			}

			@Override
			public void onSuccess(Boolean result) {
				
				Window.alert("Inizializzazione avvenuta con successo, riavviare il servizio");
				
			}
		});
	}
}
