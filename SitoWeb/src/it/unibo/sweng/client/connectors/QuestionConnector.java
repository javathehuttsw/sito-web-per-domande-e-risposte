package it.unibo.sweng.client.connectors;


// core GWT 
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
// Views
import it.unibo.sweng.client.views.AnswersListView;
import it.unibo.sweng.client.views.HomeView;
import it.unibo.sweng.client.SitoWeb;
// Services 
import it.unibo.sweng.client.services.QuestionService;
import it.unibo.sweng.client.services.QuestionServiceAsync;
// Domain objects 
import it.unibo.sweng.shared.Answer;
import it.unibo.sweng.shared.Question;
import it.unibo.sweng.shared.User;
// Util 
import java.util.ArrayList;

/**
 * QuestionConnector
 * 
 * Classe Contoller che si occupa di interagire con il servizio delle domande e risposte e gestisce i risultati
 * @author Gianmarco Spinaci
 *
 */
public class QuestionConnector {
	
	// Servizio per la gestione delle domande
	private QuestionServiceAsync questionService;
	
	// Istanza della controller lato client 
	private SitoWeb sitoInstance;
	
	// Costruttore di default
	public QuestionConnector()
	{
		this.sitoInstance = SitoWeb.getInstance();
	}
	
	/**
	 * 
	 * Invio dei parametri per creare la domanda dal client al server
	 * 
	 * @param text 			testo inserito dall'utente
	 * @param category		categoria corrente in cui si trova l'utente
	 * @param user			utente corrente che sta provando l'inserimento della domanda
	 */
	public void insertQuestion(String text, int category, User user){
		
		//controlla che il testo sia stato inserito e che sia minore di 300 caratteri
		if(text.trim().length() > 0 && text.trim().length() <= 300) {
		     
			if (questionService == null) questionService = GWT.create(QuestionService.class); 
			
			//richiama il server
			questionService.insertQuestion(text,category,user, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					
					Window.alert(caught.getMessage());
				}

				@Override
				public void onSuccess(String result) {

					//Refresha la pagina principale, passando la categoria
					sitoInstance.loadHome(sitoInstance.getCurrentCategory());
				}
			});

	    } else Window.alert("Errore: il messaggio è lungo "+text.trim().length()); 
	}

	/**
	 * Invio dei parametri per creare la risposta della domanda scelta
	 * 
	 * @param text
	 * @param question 		domanda scelta 
	 * @param user			utente corrente che sta provando l'inserimento della risposta
	 */
	public void insertAnswer(String text, Question question, User user) {
		
		// Se la risposta è tra 0 e 500 caratteri posso inserirla 
		if(text.trim().length() > 0 && text.trim().length() <= 500) {
		    
			// instanzio il servizio
			if (questionService == null) questionService = GWT.create(QuestionService.class);
			
			// faccio la chiamata asincrona 
			questionService.insertAnswer(text,question,user, new AsyncCallback<Answer>() {

				@Override
				public void onFailure(Throwable caught) {
					
					// Mostro l'errore nella view che mi ha chiamato 
					AnswersListView.setErrorLabel(caught.getMessage());
				}
				@Override
				public void onSuccess(Answer result) {
					
					// Ricarico la pagina della domanda
					sitoInstance.singleQuestionView(sitoInstance.getCurrentQuestion());
				}
			});

	    } else Window.alert("Errore: il messaggio è lungo "+text.trim().length()); 
	}
	
	/**
	 * 
	 * 
	 * 
	 * @param a
	 */
	public void updateAnswer(Answer a) {

		if (questionService == null) questionService = GWT.create(QuestionService.class);

		questionService.updateAnswer(a, new AsyncCallback<ArrayList<Answer>>() {

			@Override
			public void onFailure(Throwable caught) {
				
				// Mostro il messaggio d'errore nella view che ha chiamato il servizio
				AnswersListView.setErrorLabel(caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Answer> result) {
				
				// Refresh della lista di risposte 
				AnswersListView.setAnswersList(result);
			}
		});

	}

	/**
	 * 
	 * Ricerca tutte le risposte ad una domanda 
	 * 
	 * @param question 		domanda per cui ricercare la risposta
	 */
	public void fetchAnswers(Question question) {
		
		if (questionService == null) questionService = GWT.create(QuestionService.class);
		
		questionService.fetchAnswers(question, new AsyncCallback<ArrayList<Answer>>() {

			@Override
			public void onFailure(Throwable caught) {
				
				// Mostro l'errore nella view
				AnswersListView.setErrorLabel(caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Answer> results) {
				
				// Setto la lista delle risposte
				AnswersListView.setAnswersList(results);
			}
		});		
	}

	

	/**
	 * 
	 * Prende tutte le domande partendo dalla categoria padre e delle sotto-category
	 * 
	 * @param idCategory 	id stringa della categoria 
	 */
	public void fetchQuestions(int idCategory){
		
		if (questionService == null) questionService = GWT.create(QuestionService.class);
		
		questionService.fetchQuestions(idCategory, new AsyncCallback<ArrayList<Question>>() {

			@Override
			public void onFailure(Throwable caught) {
				
				// Mostro l'errore nella view
				HomeView.showErrors(caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Question> results) {
				
				// Setto la lista delle domande nella homepage 
				HomeView.setQuestionList(results);
			}
		});
	}
	
	/**
	 * 
	 * cancellazione della domanda
	 * 
	 * @param id 			id della domanda
	 */
	public void deleteQuestion(Question question){
		
		//controlla che la domanda sia stato specificata
		if(question != null) {
		     
			if (questionService == null) questionService = GWT.create(QuestionService.class); 
			
			//richiama il server
			questionService.deleteQuestion(question, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					
					Window.alert(caught.getMessage());
				}
				@Override
				public void onSuccess(String result) {
					
					//Ricarico la homepage
					SitoWeb.getInstance().loadHome(SitoWeb.getInstance().getCurrentCategory());
				}
			});

	    } else Window.alert("Errore: Non hai selezionato nessuna domanda"); 
	    
	}

	public void deleteAnswer(Answer answer) {
		
		if(answer != null) {
		     
			if (questionService == null) questionService = GWT.create(QuestionService.class); 
			
			//richiama il server
			questionService.deleteAnswer(answer, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					
					Window.alert(caught.getMessage());
				}
				@Override
				public void onSuccess(String result) {
					
					// ricarico la homepage
					SitoWeb.getInstance().loadHome(SitoWeb.getInstance().getCurrentCategory());
				}
			});

	    } else Window.alert("Errore: Non hai selezionato nessuna risposta"); 
	}
}