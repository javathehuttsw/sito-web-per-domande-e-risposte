package it.unibo.sweng.client.connectors;

import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.services.UserService;
import it.unibo.sweng.client.services.UserServiceAsync;
import it.unibo.sweng.client.views.UserElectView;
import it.unibo.sweng.shared.User;

import java.util.ArrayList;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * UserConnector
 * 
 * Classe Contoller che si occupa di interagire con il servizio degli utenti e di gestire i risultati
 * 
 * @author Gianmarco Spinaci
 *
 */
public class UserConnector {
	
	UserServiceAsync userService;

	/**
	 * 
	 * Scambia con il server le informazioni dell'utente
	 * 
	 * @param data		Invia una Map<key,value> per evitare i NullPointerException
	 */
	public void sendRegistrationData(Map<String, String> data){
		  
		if (userService == null) userService = GWT.create(UserService.class);
		
		// Chiamo il servizio di registrazione
		userService.register(data, new AsyncCallback<User>() {
		
			@Override
			public void onFailure(Throwable caught) {
				
				Window.alert(caught.getMessage());
			}
		
			@Override
			public void onSuccess(User result) {
				
				Window.alert("Grazie per esserti registrato utente "+result.getEmail());
				// Setto l'utente loggato e ricarico la homepage
				SitoWeb.getInstance().setCurrentUser(result);
				SitoWeb.getInstance().loadHome(SitoWeb.getInstance().getCurrentCategory());
			}
		});
	}
	
	/**
	 * 
	 * Effettua il login dell'utente 
	 * 
	 * @param data
	 */
	public void sendLoginData(Map<String, String> data){
		  
		if (userService == null) {
		  
			userService = GWT.create(UserService.class);
		}
	  
		// Chiamo il servizio di login 
		userService.login(data.get("email"),data.get("password"), new AsyncCallback<User>() {
		
			@Override
			public void onFailure(Throwable caught) {
				
				Window.alert(caught.getMessage());
			}
		
			@Override
			public void onSuccess(User result) {
				// Mostro il risultato in base al tipo
				if(result.isAdmin())
					Window.alert("Bentornato amministratore "+result.getUsername());
				else if(result.isJudge())
					Window.alert("Bentornato giudice "+result.getUsername());
				else
					Window.alert("Bentornato utente "+result.getUsername());
				
				// Setto l'utente e ricarico la homepage
				SitoWeb.getInstance().setCurrentUser(result);
				SitoWeb.getInstance().loadHomePage();
			}
		});
	}
	
	// Prendo la lista di utenti
	public void fetchUsers(){
		  
		if (userService == null) {
		  
			userService = GWT.create(UserService.class);
		}
	  
		// Chiamo il servizio per la lista di utenti
		userService.fetchUsers(new AsyncCallback<ArrayList<User>>() {
		
			@Override
			public void onFailure(Throwable caught) {
				
				Window.alert(caught.getMessage());
			}
		
			@Override
			public void onSuccess(ArrayList<User> result) {
				// Setto la lista di utenti.
				UserElectView.setUserList(result);
			}
		});
	}

	// Elezione di un giudice
	public void electJudge(User u) {
		
		if (userService == null) userService = GWT.create(UserService.class);
		
		// Chiamo il servizio di elezione di un giudice
		userService.electJudge(u, new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
				
				Window.alert(caught.getMessage());
			}

			@Override
			public void onSuccess(User result) {
				//ricarico la pagina
				Window.alert("Utente eletto con successo");
				SitoWeb.getInstance().loadHome(SitoWeb.getInstance().getCurrentCategory());
			}
		});
	}
	
}
