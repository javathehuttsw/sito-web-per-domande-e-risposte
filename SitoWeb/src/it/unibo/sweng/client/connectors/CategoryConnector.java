package it.unibo.sweng.client.connectors;

// Utils
import java.util.ArrayList;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import it.unibo.sweng.client.SitoWeb;
// Services
import it.unibo.sweng.client.services.CategoryService;
import it.unibo.sweng.client.services.CategoryServiceAsync;
// View
import it.unibo.sweng.client.views.CategoryAddView;
import it.unibo.sweng.client.views.HomeView;
import it.unibo.sweng.client.views.CategoryRenameView;
// Category
import it.unibo.sweng.shared.Category;

/**
 * CategoryConnector
 * 
 * Classe Contoller che si occupa di interagire con il servizio delle categorie e gestire i risultati
 * 
 * @author Filippo Boiani
 *
 */
public class CategoryConnector {
	
	// Servizio 
	private CategoryServiceAsync categoryService;
	
	// Istanza del Controller 
	private SitoWeb sitoInstance;
	
	// Prendo l'istanza del Controller 
	public CategoryConnector()
	{
		this.sitoInstance = SitoWeb.getInstance();
	}
	
	/**
	 * 
	 * Cerca tutte le sotto-categorie data una categoria
	 * 
	 * @param category 		padre, da cui scorrere 
	 */
	public void fetchCategories(int category)
	{
		if (categoryService == null) categoryService = GWT.create(CategoryService.class);
		// Prendo la lista di categorie
		categoryService.fetchCategories(category, new AsyncCallback<ArrayList<Category>>() {

			@Override
			public void onFailure(Throwable caught) {
				
				Window.alert(caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Category> result) {
				// Setto la lista di categorie
				SitoWeb.getInstance().categories = result;
				HomeView.setCategoryList(result);
			}
		});
		
	}
	
	/**
	 * 
	 * Richiede al server la lista di tutte le categorie
	 * 
	 */
	public void fetchAllCategories()
	{
		if (categoryService == null) categoryService = GWT.create(CategoryService.class);
		
		// Prendo tutte le categorie 
		categoryService.fetchAllCategories(new AsyncCallback<ArrayList<Category>>() {
			
			@Override
			public void onSuccess(ArrayList<Category> result) {
				
				//richiama RenameCategoryView e inserisce la lista dei risultati dentro la listbox
				CategoryRenameView.setRenameCategoryPanel(result);
				CategoryAddView.setAddCategoryPanel(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
				
			}
		});
	}
	
	/**
	 * 
	 * @param name
	 * @param category
	 */
	public void renameCategory(String name, Category category)
	{
		if(name.length() > 0)
		{
			if (categoryService == null) categoryService = GWT.create(CategoryService.class);
			// Chiamo il servizio di rinomina delle categorie
			categoryService.renameCategory(name, category, new AsyncCallback<ArrayList<Category>>() {

				@Override
				public void onSuccess(ArrayList<Category> result) {
					
					Window.alert("Cateogria rinominata");
					
					// Pulisco e salvo le nuove categorie
					sitoInstance.categories.clear();
					sitoInstance.categories = result;
					
					// Ricarico la homepage
					sitoInstance.loadHomePage();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					Window.alert(caught.getMessage());
				}
			});
		} else Window.alert("Errore, inserire il nuovo nome della domanda");
		
	}
	
	/**
	 * 
	 * @param name
	 * @param category
	 */
	public void insertCategory(String name, Category father)
	{
		if(name.length() > 0)
		{
			if (categoryService == null) categoryService = GWT.create(CategoryService.class);
			// Chiamo il servizio di aggiunta di una categoria 
			categoryService.insertCategory(name, father, new AsyncCallback<Category>() {

				@Override
				public void onSuccess(Category result) {
					
					Window.alert("Aggiunto id: "+result.getId());
					// Pulisco e salvo le nuove categorie
					sitoInstance.categories.clear();
					// Ricarico la homepage
					sitoInstance.loadHomePage();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					Window.alert(caught.getMessage());
				}
			});
		} else Window.alert("Errore, inserire il nome di una categoria");
	}
}
