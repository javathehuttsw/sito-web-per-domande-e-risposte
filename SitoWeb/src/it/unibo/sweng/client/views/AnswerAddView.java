package it.unibo.sweng.client.views;

import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.connectors.QuestionConnector;
import it.unibo.sweng.shared.Question;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe AnswerAddView.
 * 
 * View per l'aggiunta di una risposta. 
 * 
 * @author Gianmarco Spinaci
 *
 */
public class AnswerAddView {
	
	final Label welcomeMsg;
    final VerticalPanel verticalPanel;
    
    TextArea txtQuestion;
    Button submit;
    
    Question question;
    
    
    /**
     * Costruisce il pannello della creazione della risposta
     * @param question		Domanda corrente
     */
    public AnswerAddView(Question question){
    	
    	this.question = question;
    	
    	welcomeMsg = new Label("Aggiungi una risposta");
        verticalPanel= new VerticalPanel();
        verticalPanel.setStyleName("col-md-6 col-xs-12 col-sm-6 addAnswerPanel");
     
        txtQuestion = new TextArea();
        submit = new Button("Submit answer");
        submit.setStyleName("btn btn-primary-outline");
    }
    
    /**
     * Assembla e ritorna il pannello verticale
     * @return				Pannello verticale costruito, da aggiungere al rootpanel		
     */
    public VerticalPanel render() {
        
    	verticalPanel.add(welcomeMsg);
        verticalPanel.add(txtQuestion);
        verticalPanel.add(submit);
        
        setHandlers();
        
        return verticalPanel;
    }
       
    // Imposta l'handler del bottone
    public void setHandlers()
	{
    	submit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// Chiama il controller delle view QuestionConnector
				new QuestionConnector().insertAnswer(txtQuestion.getText(), question, SitoWeb.getInstance().getCurrentUser());
			}
		});
	}
}
