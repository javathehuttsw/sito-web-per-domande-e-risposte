package it.unibo.sweng.client.views;

import it.unibo.sweng.client.connectors.*;
import it.unibo.sweng.shared.User;
import java.util.ArrayList;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * UserElectView
 * 
 * View per l'elezione di un giudice
 * 
 * @author Alessandro Leone
 *
 */
public class UserElectView
{	
	UserElectView view = this;
	private static final ListBox listBoxUser = new ListBox();
	
	private static Label errorLabel = new Label("");
	private Button btnRedirect;
	
	private HorizontalPanel hpanel;
	
	private static ArrayList<User> listUsers;
	
	// Costruttore
	public UserElectView() {
		
		listUsers = new ArrayList<User>();
		
		// chiamo la lista delle categorie
		new UserConnector().fetchUsers();
		
		hpanel = new HorizontalPanel();
		
		btnRedirect = new Button("Nomina");
				
	}

	// Setto la view 
	public VerticalPanel render() {
		
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Seleziona un giudice:"));		
		hpanel.add(listBoxUser);
		
		btnRedirect.setStyleName("btn btn-primary-outline");
		hpanel.add(btnRedirect);
		
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setStyleName("col-md-6 col-xs-12 col-sm-6");
		vPanel.add(hpanel);
		
		vPanel.add(errorLabel);
		// Setto gli handler 
		setHandlers();
		
		return vPanel;
	}
	
	// Setto la lista di utenti nella listbox 
	public static void setUserList( ArrayList<User> list ) {
		listBoxUser.clear();
		if(list == null || list.size() == 0 ){
			// Se non ci sono utenti 
			listBoxUser.addItem("Nessun utente disponibile");
			
		}else {
			listUsers = list;
			for(User c: list){
				listBoxUser.addItem(c.getUsername());
			}
		}
		
	}
	
	// Mostro gli errori
	public static void showErrors(String text)
	{
		errorLabel.setStyleName("error-label");
		errorLabel.setText(text);
	}
	
	// Setto gli handler 
	public void setHandlers()
	{
		btnRedirect.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(listUsers.size() != 0){
					User u = listUsers.get(listBoxUser.getSelectedIndex());
					new UserConnector().electJudge(u);
				}else {
					showErrors("Nessun utente selezionato");
				}
			}
		});

	}

}
