package it.unibo.sweng.client.views;

import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.connectors.QuestionConnector;
import it.unibo.sweng.shared.Question;
import it.unibo.sweng.shared.User;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe QuestionCustomCard
 * 
 * Rappresenta la card con la domanda. 
 * Ha lo scopo di gestire la domanda e gli eventi che vengono generati (redirect e cancellazione)
 * 
 * Si occupa inoltre dei diritti di visualizzazione. 
 * 
 * @author Filippo Boiani
 *
 */
public class QuestionCustomCard extends Composite implements HasClickHandlers
{
	// Elementi della card
	private Label text = new Label();
	private Label date = new Label();
	private Label author = new Label();
	private Label category = new Label();
	private Button deleteButton = new Button("X");
	// Domanda della card 
	private Question q; 

	private VerticalPanel info = new VerticalPanel();
	private HorizontalPanel card = new HorizontalPanel();
	
	// Costruttore: setta la question e la card 
	public QuestionCustomCard(Question q) {
		this.q =  q; 
		this.setCard(q);
	}
	
	/**
	 * Setta la card 
	 * @param question
	 */
	@SuppressWarnings("deprecation")
	private void setCard(Question question) {
		text.setText(question.getText());
		date.setText(DateTimeFormat.getMediumDateTimeFormat().format(question.getDate()));
		author.setText("Posted by "+question.getCreator().getUsername());
		category.setText(question.getCategory().getName());
		
		date.setStyleName("text-muted small");
		author.setStyleName("text-muted small");
		category.setStyleName("label label-primary small");
		
		// Clicco sul testo e vengo reindirizzato alla pagina della domadna
		text.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Visualizza la domanda");
				// redirect alla pagina della domanda
				SitoWeb.getInstance().singleQuestionView(q);
			}
		});
		
		// Prendo lo user logato
		User currentUser = SitoWeb.getInstance().getCurrentUser();
		
		// Se è admin può eiminare la domanda
		if(currentUser != null && currentUser.isAdmin() ) {
			deleteButton.setStyleName("btn label label-pill label-danger small delete-button");
			
			deleteButton.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					GWT.log("Cancella la domanda");
					// Chiamata al controller della view 
					new QuestionConnector().deleteQuestion(q);
				}
			});
			
			card.add(deleteButton);
		}
		info.add(text);
		info.add(date);
		info.add(author);
		info.add(category);
		
		card.setStyleName("card card-block card-inverse");
		
		
		card.add(info);
		// Setta il widget per il composite
		initWidget(card);
		
	}
	
	// Prende la question
	public Question getQuestion(){
		return q;
	}
	
	// Prende il bottone delete (per settare gli handler)
	public Button getDeleteButton(){
		return deleteButton;
	}
	
	// Prende la label del testo (per settare gli handler)
	public Label getQuestionLabel(){
		return text;
	}
	
	// handler del click
	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return addDomHandler(handler, ClickEvent.getType());
	}

}
