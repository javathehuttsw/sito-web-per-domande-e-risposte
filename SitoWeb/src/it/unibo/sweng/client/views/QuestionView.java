package it.unibo.sweng.client.views;

import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.connectors.QuestionConnector;
import it.unibo.sweng.shared.Category;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * QuestionView
 * 
 * view per l'aggiunta di una domanda (non utilizzata, si veda QuestionAddView)
 * @author Gianmarco Spinaci
 *
 */
public class QuestionView {

	final Label welcomeMsg;
    final VerticalPanel verticalPanel;
 
    TextArea txtQuestion;
    TextBox txtCategory;
    Button submit;
    
    List<Category> categories;
 
    // Costruttore
    public QuestionView(){
    	
    	welcomeMsg = new Label("Scrivi la domanda (massimo 300 caratteri)");
        verticalPanel= new VerticalPanel();
        verticalPanel.setStyleName("col-md-6 col-xs-12 col-sm-6");
     
        txtQuestion = new TextArea();
        submit = new Button("Inserisci la domanda");
        submit.setStyleName("btn btn-primary-outline");
    }
    
    // Restiutisce il pannello 
    public VerticalPanel renderQuestionSubmit() {
        
    	verticalPanel.add(welcomeMsg);
        verticalPanel.add(txtQuestion);
        //verticalPanel.add(txtCategory);
        verticalPanel.add(submit);
        
        setHandlers();
        
        return verticalPanel;
        
    }
    
    // Setta gli handler
    public void setHandlers()
	{
    	submit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				// Inserisce la domanda
				new QuestionConnector().insertQuestion(
						txtQuestion.getText(), 
						SitoWeb.getInstance().getCurrentCategory().getId(), 
						SitoWeb.getInstance().getCurrentUser()
					);
				
			}
		});
	}
}