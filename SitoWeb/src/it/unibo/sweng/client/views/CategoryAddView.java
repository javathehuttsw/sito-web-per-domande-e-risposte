package it.unibo.sweng.client.views;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import it.unibo.sweng.client.connectors.CategoryConnector;
import it.unibo.sweng.shared.Category;

/**
 * CategoryAddView
 * 
 * View dell'aggiunta di una categoria. 
 * 
 * @author Alessandro Leone
 *
 */
public class CategoryAddView {
	
	TextBox txtCategory= new TextBox();
	final VerticalPanel info = new VerticalPanel();
	static final ListBox listBoxCategory = new ListBox();
	
	static Label errorLabel = new Label("");
	Button btnRedirect;
	
	final HorizontalPanel hpanel;
	final VerticalPanel vPanel;
	
	// Lista delle categorie
	static ArrayList<Category> catList;
	
	// Costruttore 
	public CategoryAddView() {
		
		// chiamo la lista delle categorie
		new CategoryConnector().fetchAllCategories();
		
		hpanel = new HorizontalPanel();
		
		btnRedirect = new Button("inserisci categoria");
		
		vPanel = new VerticalPanel();
				
	}

	/**
     * 
     * Assembla e ritorna il pannello verticale
     * 
     * @return				Pannello verticale costruito, da aggiungere al rootpanel		
     */
	public VerticalPanel render() {
		
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Aggiungi categoria:"));
		hpanel.add(txtCategory);
		
		hpanel.add(new Label("Seleziona categoria padre:"));
		hpanel.add(listBoxCategory);
		
		btnRedirect = new Button("inserisci categoria");
		btnRedirect.setStyleName("btn btn-primary-outline");
		hpanel.add(btnRedirect);
		
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setStyleName("col-md-6 col-xs-12 col-sm-6");
		vPanel.add(hpanel);
		
		vPanel.add(errorLabel);
		
		// Setto gli handler
		setHandlers();
		
		return vPanel;
	}
	
	// Setto la listbox con le categorie
	public static void setAddCategoryPanel( ArrayList<Category> list ) {
		listBoxCategory.clear();
		catList = list;
		for(Category c: list){
			listBoxCategory.addItem(c.getName());
		}
	}
	
	// Mostra gli errori 
	public static void showErrors(String text)
	{
		errorLabel.setText(text);
	}
	
	// Setto gli handler
	public void setHandlers()
	{
		// Gestore del click del bottone
		btnRedirect.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// Prendo il nome 
				String newCategoryName = txtCategory.getText();
				// Prendo la cateogoria
				Category c = catList.get(listBoxCategory.getSelectedIndex());
				
				if(newCategoryName.length() > 0 ){
					new CategoryConnector().insertCategory( newCategoryName, c);
				}
			}
		});

	}

}