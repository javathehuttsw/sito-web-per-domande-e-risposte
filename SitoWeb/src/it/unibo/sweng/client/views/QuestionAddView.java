package it.unibo.sweng.client.views;

import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.connectors.QuestionConnector;
import it.unibo.sweng.shared.Category;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;

import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * View di aggiunta di una domanda
 * @author Filippo Boiani
 *
 */
public class QuestionAddView {

	final Label welcomeMsg;
    final VerticalPanel verticalPanel;
 
    TextArea txtQuestion;
    TextBox txtCategory;
    Button submit;
    
    List<Category> categories;
 
    // Costruttore: costruisce la view
    public QuestionAddView(){
    	
    	welcomeMsg = new Label("Scrivi la domanda (massimo 300 caratteri)");
        verticalPanel= new VerticalPanel();
        verticalPanel.setStyleName("col-md-6 col-xs-12 col-sm-6");
     
        txtQuestion = new TextArea();
        submit = new Button("Inserisci la domanda");
        submit.setStyleName("btn btn-primary-outline");
    }
    
    //Setta la view e restituisce il pannello
    public VerticalPanel renderQuestionSubmit() {
        
    	verticalPanel.add(welcomeMsg);
        verticalPanel.add(txtQuestion);
        verticalPanel.add(submit);
        
        setHandlers();
        
        return verticalPanel;
        
    }
    
    // Setto gli handler
    public void setHandlers()
	{
    	submit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				// Chiamo il controller per l'inserimento
				new QuestionConnector().insertQuestion(
						
						txtQuestion.getText(), 
						//un altro modo per fare sta cosa?
						SitoWeb.getInstance().getCurrentCategory().getId(), 
						SitoWeb.getInstance().getCurrentUser()
					);
				
			}
		});
	}

}