package it.unibo.sweng.client.views;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.connectors.CategoryConnector;
import it.unibo.sweng.client.connectors.QuestionConnector;
import it.unibo.sweng.shared.Category;
import it.unibo.sweng.shared.Question;
import it.unibo.sweng.shared.User;

/**
 * HomeView
 * 
 * Rappresneta la view della Home
 * 
 * @author Gianmarco Spinaci
 *
 */
public class HomeView{

	public static final ListBox categoryBox = new ListBox();
	
	// Pannello con le card delle domande
	public static final VerticalPanel questionsPanel = new VerticalPanel();
	public static final VerticalPanel categoriesPanel = new VerticalPanel();
	public static final VerticalPanel optionsPanel = new VerticalPanel();
	
	// Lista delle card delle domande
	public static ArrayList<QuestionCustomCard> questionsCardList = null;
	
	// Lista di question e categorie
	public static List<Question> questionList = null; 
	public static List<Category> categoriesList = null;
	
	// Breadcrumb
	final HorizontalPanel breadcrumb = new HorizontalPanel();
	
	/**
	 *
	 * Accetta una cateoria come parametro d'ingresso per settare la lista delle domande. 
	 * 
	 * @param category
	 */
	public HomeView(Category category){
		
		initInfo(category);
	}
	
	/**
	 * 
	 * Ritorna il modulo delle domande. 
	 * 
	 * @return
	 */
	public VerticalPanel renderQuestions() {
    	// fill the vertical Panel
        // set handlers
        return questionsPanel;   
    }
	
	/**
	 * 
	 * Ritorna il modulo della selezione di categorie. 
	 * 
	 * @return
	 */
	public VerticalPanel renderCategories() {
        return categoriesPanel;   
    }
	
	/**
	 * 
	 * Setta il controller di questa view.
	 * 
	 * @param category
	 */
	private void initInfo(Category category) {
		
		// Istanzio i controller
		QuestionConnector connectorQ = new QuestionConnector();
		CategoryConnector connectorC = new CategoryConnector();
		
		// Prendo la lista di categorie e di domande
		connectorC.fetchCategories(category.getId());
		connectorQ.fetchQuestions(category.getId());		
    }
	
	/**
	 * 
	 * Setta la lista delle domande nel modulo. 
	 * 
	 * @param list			Lista di domande che torna dal server
	 */
	public static void setQuestionList(List<Question> list){
    	
    	questionsCardList = new ArrayList<>();
    	
    	questionList = list; 
    	questionsPanel.clear();
    	
    	if(!questionList.isEmpty()){
    		
    		//Per ogni domanda crea una questionCard e aggiunge il ClickHandler
    		for(Question q: list){
    			
    			QuestionCustomCard cqc = new QuestionCustomCard(q);
    			questionsCardList.add(cqc);
        		
        		questionsPanel.add(cqc);
        	}
    	}
    	else{
    		questionsPanel.clear();
    		Label tmpLabel = new Label("Non ci sono domande!");
    		questionsPanel.add(tmpLabel);
    		
    	}
    } 
    
	/**
	 * Setta la lista di categorie nel Box di scelta. 
	 * 
	 * @param list			Lista di categorie che torna dal server
	 */
	public static void setCategoryList(List<Category> list)
	{
		categoriesList = list; 
		
		//pulisco i pannelli
		categoriesPanel.clear();
		categoryBox.clear();
   
		//aggiungo tutte le categorie, tranne la prima, al category box
		for(int i = 1; i<list.size(); i++)
		{
			categoryBox.addItem(list.get(i).getName());
		}

		//aggiungo i widget al pannello
		categoriesPanel.add(new Label("Categorie : "));
		categoriesPanel.add(categoryBox);
		
		//creo e setto logiche ai bottoni
		Button tmpButton = new Button("Cambia categoria");
		Button homeButton = new Button("Torna all'inizio");
		
		homeButton.setStyleName("btn btn-danger btn-block");
		tmpButton.setStyleName("btn btn-success btn-block");
		
		tmpButton.addClickHandler( new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				//refresh della pagina principale, con la nuova categoria selezionata
				SitoWeb.getInstance().loadHome(categoriesList.get(categoryBox.getSelectedIndex()+1));
			}
		});
		
		homeButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				//carico la pagina principale
				SitoWeb.getInstance().loadHomePage();
			}
		});
		
		categoriesPanel.add(tmpButton);
		categoriesPanel.add(homeButton);
	}
	
	/**
	 * 
	 * Mosta gli errori. 
	 * 
	 * @param text			Testo dell'errore throwable catturato nel server
	 */
	public static void showErrors(String text)
	{
		questionsPanel.clear();
		questionsPanel.add(new Label(text));
	}
	
	/**
	 * 
	 * Imposta il pannello verticale per l'inserimento della domanda
	 * 
	 * @return				Pannello con il bottone inserisci domanda
	 */
	public VerticalPanel renderOptions()
	{
		User user = SitoWeb.getInstance().getCurrentUser();
		
		optionsPanel.clear();
		
		if( SitoWeb.getInstance().getCurrentCategory().getId() != 0 )
		{
			Button button = new Button("Inserisci nuova domanda");
			button.setStyleName("btn btn-warning-outline");
			
			button.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					
					//visualizza la pagina di inserimento nuova domanda
					SitoWeb.getInstance().questionSubmit();
					
				}
			});
			optionsPanel.add(button);
		}
		
		// Se è admin mostro l'opzione di nomina giudice
		if(user.isAdmin())
		{
			Button electJudge = new Button("Nomina giudice");
			electJudge.setStyleName("btn btn-success-outline");
			electJudge.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					
					//visualizza la pagina di inserimento nuova domanda
					SitoWeb.getInstance().electJudge();
					
				}
			});
			optionsPanel.add(electJudge);
		}
		
		// Se è admin mostro l'opzinoe di rinomina di una categoria 
		if(user.isAdmin())
		{
			Button renameCategory = new Button("Rinomina categorie");
			renameCategory.setStyleName("btn btn-primary-outline");
			renameCategory.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					
					//visualizza la pagina per rinominare la categoria
					SitoWeb.getInstance().renameCategory();
					
				}
			});
			optionsPanel.add(renameCategory);
		}
		
		// Se è admin mostro l'opzione di aggiunta di una categoria
		if(user.isAdmin())
		{
			Button addCategory = new Button("Inserisci categoria");
			addCategory.setStyleName("btn btn-success-outline");
			addCategory.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					
					//visualizza la pagina di inserimento nuova domanda
					SitoWeb.getInstance().insertCategory();
					
				}
			});
			optionsPanel.add(addCategory);
		}
		
		return optionsPanel;
	}
	
	
	
	
	/**
	 * Mostra i BreadCrumbs correnti
	 * 
	 * @param category		
	 * @return				Label con la lista delle categorie
	 */
	public Widget renderBreadCrumb(Category category)
	{
		RootPanel.get("breadcrumb").clear();
		
		GWT.log("cat name: "+category.getName());
		GWT.log("cat father: "+category.getFather()+"");
		
		ArrayList<Category> appoggio = new ArrayList<Category>();

		int count = 1;
		Category lean = category;
		appoggio.add(lean);

		//prendo la launghezza della lista
		while( lean.getFather() != null && count < 5){
			
			lean = lean.getFather();
			appoggio.add(lean);
			//GWT.log(lean.getName());
			count++;
		}
		GWT.log("count: "+count);
		final Category[] catpath = new Category[appoggio.size()];
		
		lean = category;
		if( count != 0 ){
			GWT.log("ciclo");
			int i = appoggio.size()-1;
			
			for(Category c: appoggio){
				catpath[i] = c; 
				i--;
			}
			
			for(int j= 0; j< catpath.length; j++){
				
				String catName = catpath[j].getName().toLowerCase().equals("r".trim()) || catpath[j].getName().toLowerCase().equals("root".trim()) ?
								"home" : 
								">   "+catpath[j].getName().toLowerCase();
				
				Label label = new Label(catName);
				label.addClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						
						//String id = ((Label) event.getSource()).getText();

						int index = breadcrumb.getWidgetIndex((Label) event.getSource());
						//Window.alert(catpath[index].getName());
						SitoWeb.getInstance().loadHome(catpath[index]);
						
						
					}
				});
				breadcrumb.add(label);
			}
		}else {
			Label label = new Label("Home");
			breadcrumb.add(label);
		}
		
		breadcrumb.setStyleName("breadcrumb");
		
		return breadcrumb;
	}
	
	/**
	 * 
	 * se l'utente non esiste, visualizza un bottone
	 * se l'utente esiste visualizza il suo nome
	 * 
	 * @param user			Utente che fa la richiesta
	 * @return				Widget (Button || Label)
	 */
	public Widget renderLoginButton(User user)
	{
		RootPanel.get("btnLogin").clear();
		
		if(user == null)
		{
			Button button = new Button("effettua login");
			
			button.setStyleName("btn btn-success");
			button.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					
					SitoWeb.getInstance().login();
					
				}
			});
			
			return button;
		}
		else
		{
			return new Label(user.getUsername());
		}
	}
	
	/**
	 * 
	 * se l'utente non esiste, visualizza il bottone di registrazione
	 * se l'utente esiste visualizza il suo tipo
	 * 
	 * @param user			Utente che fa la richiesta
	 * @return				Widget (Button || Label)
	 */
	public Widget renderRegistrationButton(User user)
	{
		RootPanel.get("btnRegister").clear();
		
		if(user == null)
		{
			Button button = new Button("registrati");
			
			button.setStyleName("btn btn-info");
			button.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					
					SitoWeb.getInstance().registration();
					
				}
			});
			
			return button;
		}
		else
		{
			return new Label(user.getType());
		}
	}
}
