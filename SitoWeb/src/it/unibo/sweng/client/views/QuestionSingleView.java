package it.unibo.sweng.client.views;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.connectors.QuestionConnector;
import it.unibo.sweng.shared.Question;

/**
 * QuestionSingleView
 * 
 * View con la domanda e le risposte. 
 * 
 * @author Filippo Boiani
 *
 */
public class QuestionSingleView{
		
	// Domanda
	Question question;
	VerticalPanel vPanel;
	
	TextArea txtArea;
	Button btnInsertAnswer;
	
	// Card della domanda
	QuestionCustomCard cqc; 
	
	// View con la lista delle risposte
	AnswersListView answerListView;
	
	// Costruttor: setta la card
	public QuestionSingleView(Question q){
		
		question = q;
		vPanel = new VerticalPanel();
		
		// Creo la card con la domanda 
		cqc = new QuestionCustomCard(question);
    	
		//Renderizza la lista delle answer
		answerListView = new AnswersListView(question);
		
		txtArea = new TextArea();
		txtArea.setStyleName("form-control");
		
		btnInsertAnswer = new Button("Inserisci la risposta");
		
		btnInsertAnswer.setStyleName("btn btn-primary pull-lg-right");
		
		// Setto l'handler per agiungere una risposta alla domanda
		btnInsertAnswer.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// Chiamo l'handler della view 
				new QuestionConnector().insertAnswer(
						txtArea.getText(), 
						question, 
						SitoWeb.getInstance().getCurrentUser()
					);
				txtArea.setText("");
			}
		});
	}
	
    
    /**
     * Return the question panel
     * @return
     */
    public VerticalPanel render() {
    	
    	// Question
    	vPanel.add(cqc);
    	
    	//solo se l'utente è loggato, si può visualizzare il bottone e la txtArea
    	if(SitoWeb.getInstance().getCurrentUser() != null)
    	{
    		vPanel.add(txtArea);
        	
        	vPanel.add(btnInsertAnswer);
    	}
    	
    	// Answers List
    	vPanel.add(answerListView.renderErrorLabel());
    	vPanel.add(answerListView.render());
    	
        return vPanel;   
    }
}
