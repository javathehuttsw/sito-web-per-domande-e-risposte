package it.unibo.sweng.client.views;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.connectors.CategoryConnector;
import it.unibo.sweng.shared.Category;

/**
 * CategoryRenameView
 * 
 * View del rename di una categoria 
 * 
 * @author Alessandro Leone
 *
 */
public class CategoryRenameView {

	public static final ListBox allCategoriesView = new ListBox();
	
	public static final VerticalPanel renameCategoryPanel = new VerticalPanel();
	
	public static final TextBox txtCategoryName = new TextBox();
	
	// Costruttore
	public CategoryRenameView()
	{
		renameCategoryPanel.setStyleName("col-md-6 col-xs-12 col-sm-6 from");
		// Prendo la lista di categorie
		new CategoryConnector().fetchAllCategories();
	}
	
	// Setto il pannello e la listbox
	public static void setRenameCategoryPanel(ArrayList<Category> categories)
	{
		//pulisco il pannello e la textbox
		renameCategoryPanel.clear();
		txtCategoryName.setText("");
		allCategoriesView.clear();
		
		//salvo la nuova lista di categorie dentro sitoweb
		SitoWeb.getInstance().categories = categories;
		
		//inserisco ogni categoria dentro la listbox
		for(Category category : categories)
			allCategoriesView.addItem(category.getName());
		
		
		txtCategoryName.setStyleName("form-control");
		
		Button renameButton = new Button("Rinomina la categoria");
		
		renameButton.setStyleName("btn btn-primary");
		// Setto l'handler di evento
		renameButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				//categoria selezionata, in base all'indice
				int index = allCategoriesView.getSelectedIndex();
				Category category = SitoWeb.getInstance().categories.get(index);
				// Chiamo il controller della view
				new CategoryConnector().renameCategory(txtCategoryName.getText(), category);
			}
		});
		
		renameCategoryPanel.add(allCategoriesView);
		renameCategoryPanel.add(txtCategoryName);
		renameCategoryPanel.add(renameButton);
	}
	
	// Restituisce la view
	public VerticalPanel render()
	{
		return renameCategoryPanel;
	}
}
