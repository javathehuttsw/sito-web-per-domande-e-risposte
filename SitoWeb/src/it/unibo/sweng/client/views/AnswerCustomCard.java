package it.unibo.sweng.client.views;

import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.connectors.QuestionConnector;
import it.unibo.sweng.shared.Answer;
import it.unibo.sweng.shared.Judgement;
import it.unibo.sweng.shared.User;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe AnswerCustomCard
 * 
 * Rappresenta la card con la risposta ad una domanda. 
 * Ha lo scopo di gestire la risposta e gli eventi che vengono generati (aggiunta di un giudizio
 * e cencallzione) 
 * 
 * Si occupa inoltre dei diritti di visualizzazione. 
 * 
 * @author Filippo Boiani
 *
 */
public class AnswerCustomCard extends Composite implements HasClickHandlers,HasChangeHandlers
{
	// Risposta della card
	private Answer a; 
	private Label text = new Label();
	private Label date = new Label();
	private Label author = new Label();
	
	private Button deleteButton = new Button("X");

	private VerticalPanel info = new VerticalPanel();
	
	// Lista dei voti da dare alla risposta
	public ListBox judgementNumber = new ListBox();	
	
	// Lista dei giudizi dati
	private VerticalPanel judgements = new VerticalPanel();
	
	// Pannello che contiene la card
	private HorizontalPanel hpanel = new HorizontalPanel();
	
	private Label averageVote = new Label();
	private Label desc = new Label("Avg vote");
	private VerticalPanel votePanel = new VerticalPanel();
	
	/**
	 * Costruttore, accetta una Answer e setta la card.
	 * @param answer
	 */
	public AnswerCustomCard(Answer answer) {
		this.a = answer;
		
		// Prendo lo user loggato 
		User user = SitoWeb.getInstance().getCurrentUser();
		
		// Se l'utente è giudice posso visualizzare il box dei giudizi
		if(user != null && user.isJudge())
		{
			judgementNumber.addItem("-");

			for(int i = 0; i<6; i++) {
				judgementNumber.addItem(""+i);
			}
		}
		this.setCard(answer);
	}
	
	/**
	 * Setta il contenuto delle card
	 * @param answer
	 */
	@SuppressWarnings("deprecation")
	private void setCard(Answer answer){
		// Setto il testo
		text.setText(answer.getText());
		// Setto la data formattata
		date.setText(DateTimeFormat.getMediumDateTimeFormat().format(answer.getDate()));
		// Setto lo user che ha dato la risposta
		author.setText("Posted by "+answer.getCreator().getUsername());
		
		// Stampo la lista dei giudici e dei loro voti
		for(Judgement j: answer.getVoteList()){
			Label l = new Label(j.getJudge().getUsername()+ ": "+j.getVote());
			l.setStyleName("small text-muted");
			judgements.add( l );
		}
		judgements.setStyleName("judgements-list");
		
		// Gestione del voto medio
		double avg = answer.getAverageVote();
		if( avg < 0 ){
			// Non presente
			averageVote.setText("");
			desc.setText("");
		}else {
			// Se è presente lo formatto
			averageVote.setText(""+NumberFormat.getFormat("0.00").format(answer.getAverageVote()));
		}
		
		date.setStyleName("text-muted small");
		author.setStyleName("text-muted small");
		averageVote.setStyleName("big");
		
		//ListBox per il voto
		judgementNumber.setStyleName("big");
		
		info.add(text);
		info.add(date);
		info.add(author);
		info.add(judgements);
		
		desc.setStyleName("very-small text-muted");
		votePanel.add(averageVote);
		votePanel.add(desc);
		
		// Quando un giudice ha già dato il giudizio non vede la listbox
		if(SitoWeb.getInstance().getCurrentUser() != null && !answer.hasJudge(SitoWeb.getInstance().getCurrentUser())) {
			// Se l'utente è giudice può vedere il pannello per votare
			if( SitoWeb.getInstance().getCurrentUser().isJudge() ) {
				votePanel.add(judgementNumber);
			}	
		}
		
		hpanel.add(votePanel);
		hpanel.add(info);
		User currentUser = SitoWeb.getInstance().getCurrentUser();
		
		// Controllo che l'utente esiste, sia un admin o un giudice
		if(currentUser != null && ( currentUser.isAdmin() || currentUser.isJudge() )) {
			
			deleteButton.setStyleName("btn label label-pill label-danger small delete-button");
			
			deleteButton.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					GWT.log("Cancella la risposta");
					// Chiamo il controller 
					new QuestionConnector().deleteAnswer(a);
				}
			});
			
			hpanel.add(deleteButton);
		}
		
		hpanel.setStyleName("card card-block card-inverse answer-card");
		// Setto hpanel nel composite 
		initWidget(hpanel);
	}
	
	
	// Handlers 
	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return addDomHandler(handler, ClickEvent.getType());
	}

	@Override
	public HandlerRegistration addChangeHandler(ChangeHandler handler) {
		return addDomHandler(handler, ChangeEvent.getType());

	}

}
