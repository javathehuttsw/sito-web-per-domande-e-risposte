package it.unibo.sweng.client.views;

import it.unibo.sweng.client.SitoWeb;
import it.unibo.sweng.client.connectors.QuestionConnector;
import it.unibo.sweng.shared.Answer;
import it.unibo.sweng.shared.Judgement;
import it.unibo.sweng.shared.Question;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe AnswersListView
 * 
 * View che rappresenta la lista di risposte ad una domanda. 
 * 
 * @author Filippo Boiani
 *
 */
public class AnswersListView{
	
	public static ArrayList<Label> labelList = null;
	public static List<Answer> answersList = null; 
	public static final VerticalPanel answersPanel = new VerticalPanel();
	private static Label qLabel;
	
	private static Label errorLabel = new Label();; 
	
	private static AnswerCustomCard card;
	
    public AnswersListView(Question question){
    	
    	//Pulisce la lista e gli eventuali errori
    	answersPanel.clear();
    	setErrorLabel("");
    	
    	answersPanel.setStyleName("answer-panel");
    	fetchAnswers(question);
    }
    
    // Setta e ritorna la label di errore
    public Label renderErrorLabel() {
    	errorLabel.setStyleName("error-label");
        return errorLabel;   
    }
    //Ritorna il pannello delle risposte
    public VerticalPanel render() {
    	// fill the vertical Panel
        // set handlers
        return answersPanel;   
    }
    
    //Prende la lista delle risposte di una data domanda
    private void fetchAnswers(Question question) {
    	new QuestionConnector().fetchAnswers(question);
    }
    
    /**
     * Set the dataSource
     * @param list
     */
    public static void setAnswersList(List<Answer> list){
    	
    	answersPanel.clear();
    	
		labelList = new ArrayList<>();
    	answersList = list; 

    	if(list.size() > 0)
    	{
    		for(Answer q: list){
        		
        		card = new AnswerCustomCard(q);
        		
        		//TODO controllo che l'utente sia un giudice
        		
        		// Handler per l'aggiunta di un voto
        		card.addChangeHandler(new ChangeHandler() {

    				@Override
    				public void onChange(ChangeEvent event) {
    					AnswerCustomCard father = (AnswerCustomCard) event.getSource();
    					int index = answersPanel.getWidgetIndex(father);
    					Answer ans = answersList.get(index);
    					//Widget sender = (Widget) event.getSource();
    			    	int voto = father.judgementNumber.getSelectedIndex()-1; 
    					Judgement jdgmt = new Judgement(voto, SitoWeb.getInstance().getCurrentUser());
    					ans.addJudgement(jdgmt);
    					// Quando aggiungo il voto aggiorna l'answer 
    					new QuestionConnector().updateAnswer(ans);
    				}
    				
    			});
        		
        		qLabel = new Label(q.getText());
        		qLabel.setStyleName("card card-block card-inverse text-xs-center");
        		labelList.add(qLabel);
        		
           		answersPanel.add(card);
        	}
    	}
    } 
    
    // Setta la lista delle answer
    public static void setAnswersList(Answer answer){
    	// Pulisco il pannello
    	answersPanel.clear();
    	
		labelList = new ArrayList<>();
    	answersList.add(answer);

    	if(answersList.size() > 0)
    	{
    		for(Answer q: answersList){
        		
        		card = new AnswerCustomCard(q);
        		// Metto un handler di cambiamento alla card
        		card.addChangeHandler(new ChangeHandler() {

    				@Override
    				public void onChange(ChangeEvent event) {
    					// Prendo la card
    					AnswerCustomCard father = (AnswerCustomCard) event.getSource();
    					// Prendo il suo index nel panel
    					int index = answersPanel.getWidgetIndex(father);
    					// Prendo la answer dalla lista corrispondente
    					Answer ans = answersList.get(index);
    					//Widget sender = (Widget) event.getSource();
    			    	int voto = father.judgementNumber.getSelectedIndex()+1; 
    					Judgement jdgmt = new Judgement(voto, SitoWeb.getInstance().getCurrentUser());
    					ans.addJudgement(jdgmt);
    					new QuestionConnector().updateAnswer(ans);
    				}
    				
    			});
        		
        		qLabel = new Label(q.getText());
        		qLabel.setStyleName("card card-block card-inverse text-xs-center");
        		labelList.add(qLabel);
        		
        		answersPanel.add(card);
        	}
    	}
    } 
    
    // Aggiunge una answer al pannello
	public void addAnswer(Answer ans) {
		
		qLabel = new Label(ans.getText());
		qLabel.setStyleName("card card-block card-inverse text-xs-center");
		labelList.add(qLabel);
		answersList.add(ans);
		
		answersPanel.add(qLabel);
	}
	
	// Setta la label di errore
	public static void setErrorLabel(String text){
		errorLabel.setText(text);
	}
}

