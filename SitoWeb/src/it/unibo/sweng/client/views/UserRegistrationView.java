package it.unibo.sweng.client.views;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import it.unibo.sweng.client.connectors.UserConnector;

/**
 * UserRegistrationView
 * 
 * View per la registrazione
 * 
 * @author Alessandro Leone
 *
 */
public class UserRegistrationView{
	
	// Elementi della view 
	final VerticalPanel vPanel;
	HorizontalPanel hpanel;
	final Label welcomeMsg;
	final TextBox txtUsername;
	final PasswordTextBox txtPassword;
	final TextBox txtEmail;
	final TextBox txtName;
	final TextBox txtLastName;
	final RadioButton rdoGenderMale;
	final RadioButton rdoGenderWoman;
	final TextBox txtBornDate;
	final TextBox txtBornPlace;
	final TextBox txtHome;
	
	final Label infoLabel; 
	final Button btnRegister;
	
	String gender;
	
	// Costryttore: istanzia gli elementi della view
	public UserRegistrationView()
	{        
		 vPanel = new VerticalPanel();
		 vPanel.setStyleName("col-md-6 col-xs-12 col-sm-6");
		 welcomeMsg = new Label("Registration page");
		 
		 txtUsername = new TextBox();
		 txtPassword = new PasswordTextBox();
		 txtEmail = new TextBox();
		 txtName = new TextBox();
		 txtLastName = new TextBox();
		 rdoGenderMale = new RadioButton("radioGroup","Maschio");
		 rdoGenderMale.setValue(true);
		 rdoGenderWoman = new RadioButton("radioGroup","Femmina");
		 txtBornDate = new TextBox();
		 txtBornPlace = new TextBox();
		 txtHome = new TextBox();

		 infoLabel = new Label();
		 
		 btnRegister = new Button("Effettua registrazione");
		 
		 gender = new String("Male");
		 
		 btnRegister.setStyleName("btn btn-primary-outline");
		 
		 txtUsername.setStyleName("form-control");
		 txtPassword.setStyleName("form-control");
		 txtEmail.setStyleName("form-control");
		 txtName.setStyleName("form-control");
		 txtLastName.setStyleName("form-control");
		 rdoGenderMale.setStyleName("form-control");
		 rdoGenderWoman.setStyleName("form-control");
		 txtBornDate.setStyleName("form-control");
		 txtBornPlace.setStyleName("form-control");
		 txtHome.setStyleName("form-control");
	}
	
	// Ritonra la view dopo aver fatto altri setting
	public VerticalPanel renderRegistration()
	{	
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Username:"));
		hpanel.add(txtUsername);
		vPanel.add(hpanel);
  
		//password
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Password:"));
		hpanel.add(txtPassword);
		vPanel.add(hpanel);
  
		//email
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Email:"));
		hpanel.add(txtEmail);
		vPanel.add(hpanel);
		
		//name
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Nome:"));
		hpanel.add(txtName);
		vPanel.add(hpanel);
				
		//lastName
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Cognome:"));
		hpanel.add(txtLastName);
		vPanel.add(hpanel);
		
		//gender
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.setSpacing(10);
		hpanel.add(new Label("Sesso:"));
		hpanel.add(rdoGenderMale);
		hpanel.add(rdoGenderWoman);
		vPanel.add(hpanel);
		
		//bornDate
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Data di nascita(gg/mm/aa):"));
		hpanel.add(txtBornDate);
		vPanel.add(hpanel);
		
		//bornPlace
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Luogo di nascita:"));
		hpanel.add(txtBornPlace);
		vPanel.add(hpanel);
		
		//home
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Residenza:"));
		hpanel.add(txtHome);
		vPanel.add(hpanel);
		
  
		vPanel.add(btnRegister);
		vPanel.add(infoLabel);
		
		// Setto gli handler
		setHandlers();
		
		return vPanel;
	}
	
	// Setta gli handler
	public void setHandlers()
	{
		// handler del bottone registrazione 
		btnRegister.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				Map<String, String> map;

				map = new HashMap<String, String>();
				
				String err= "";
				String userBox = txtUsername.getText().trim(),
						emailBox = txtEmail.getText().trim(), 
						passBox = txtPassword.getText().trim(),
						bornBox = txtBornDate.getText().trim();
				
				// Faccio dei controlli sugli input 
				if( userBox == "" ) {
					err += "Inserire l'username\n";
				}
				if( emailBox == "" ) {
					err += "Inserire l'email\n";
				} else {
				    if (!emailBox.matches("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$")) {
				        err = "Inserire l'email nel formato corretto\n";
				    }
				}
				if( passBox == "" ) {
					err += "Inserire la password\n";
				}
				
				if( bornBox != "" ) {
				    if (bornBox.matches("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)")) {
				        err += "Inserire la data di nascita nel formato corretto\n";
				    }
				}
				
				if(err == "") {
					
					map.put("username", userBox);
					map.put("password", passBox);
					map.put("email", emailBox);
					map.put("name", txtName.getText().trim());
					map.put("lastName", txtLastName.getText().trim());
					map.put("gender", gender);				
					map.put("bornDate", bornBox);
					map.put("bornPlace", txtBornPlace.getText().trim());
					map.put("home", txtHome.getText().trim());
					// Chiamo il conroller della view 
					new UserConnector().sendRegistrationData(map);
				} else {
					Window.alert(err);
				}
			}
		});
		
		// Handler del radio button del sesso uomo
		rdoGenderMale.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				gender = rdoGenderMale.getText();
				
			}
		});
		
		// Handler del radio button del sesso donna
		rdoGenderWoman.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				gender = rdoGenderWoman.getText();
				
			}
		});
	}
}
