package it.unibo.sweng.client.views;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import it.unibo.sweng.client.connectors.UserConnector;

/**
 * UserLoginView
 * 
 * View per il login 
 * 
 * @author Alessandro Leone 
 *
 */
public class UserLoginView {
	
	// Elementi della view 
	final VerticalPanel vPanel;
	HorizontalPanel hpanel;
	final Label welcomeMsg;
	final PasswordTextBox txtPassword;
	final TextBox txtEmail;
	
	final Label infoLabel; 
	final Button btnLogin;
	
	// Costruttore: instanzia gli elementi 
	public UserLoginView()
	{        
		 vPanel = new VerticalPanel();
		 vPanel.setStyleName("col-md-6 col-xs-12 col-sm-6 ");
		 welcomeMsg = new Label("Login page");
		 
		 txtPassword = new PasswordTextBox();
		 txtEmail = new TextBox();

		 infoLabel = new Label();
		 
		 btnLogin = new Button("Login");

		 btnLogin.setStyleName("btn btn-primary-outline");
		 
		 txtPassword.setStyleName("form-control");
		 txtEmail.setStyleName("form-control");

	}
	
	// Setta gli elementi, gli handler e ritorna il pannello
	public VerticalPanel renderLogin()
	{	
  
		//email
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Email:"));
		hpanel.add(txtEmail);
		vPanel.add(hpanel);
		
		//password
		hpanel = new HorizontalPanel();
		hpanel.setStyleName("form-group");
		hpanel.add(new Label("Password:"));
		hpanel.add(txtPassword);
		vPanel.add(hpanel);
		
  		vPanel.add(btnLogin);
		vPanel.add(infoLabel);
		
		// Setta gli handler 
		setHandlers();
		
		//return value
		return vPanel;
	}

	// Setta gli handler 
	public void setHandlers()
	{
		btnLogin.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				if(txtPassword.getText() != "" && txtEmail.getText() != "") {
				
					Map<String, String> map;
	
					map = new HashMap<String, String>();
					
					map.put("password", txtPassword.getText());
					map.put("email", txtEmail.getText());
					// Chiama il connettore al servizio
					new UserConnector().sendLoginData(map);
				} else {
					Window.alert("Riempire i due campi");
				}
			}
		});
		
	}
}
