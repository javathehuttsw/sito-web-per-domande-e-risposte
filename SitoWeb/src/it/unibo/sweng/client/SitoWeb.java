 package it.unibo.sweng.client;

import it.unibo.sweng.client.connectors.SitoWebConnector;
// Views
import it.unibo.sweng.client.views.CategoryAddView;
import it.unibo.sweng.client.views.UserElectView;
import it.unibo.sweng.client.views.HomeView;
import it.unibo.sweng.client.views.UserLoginView;
import it.unibo.sweng.client.views.QuestionView;
import it.unibo.sweng.client.views.UserRegistrationView;
import it.unibo.sweng.client.views.CategoryRenameView;
import it.unibo.sweng.client.views.QuestionSingleView;
// Domain Objects 
import it.unibo.sweng.shared.Category;
import it.unibo.sweng.shared.Question;
import it.unibo.sweng.shared.User;

// Utils
import java.util.ArrayList;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point del sito. 
 * @author Filippo Boiani
 *
 */
public class SitoWeb implements EntryPoint {

	// Istanza dell'entry point a cui accedere
	private static SitoWeb instance; 
	
	//categoria e utente corrente
	private Category currentCategory;
	private User currentUser;
	private Question currentQuestion;
	
	public ArrayList<Category> categories;
	
	//view della home
	private HomeView view;
	
	//pulisce le scena principale
	private void cleanScene()
	{
		RootPanel.get("app").clear();
	}
	
	// Al caricamento del modulo
	public void onModuleLoad() {
		
		// Setta il Singleton di SitoWeb che verrà chiamato da tutti i controller delle view
		instance = this;
		
		
		//###############################
		//scommentare questa riga per usare il progetto
		loadHomePage();
		
		//scommentare questa riga per inizializzare il database
		//new SitoWebConnector().initServer();
	}
	
	/**
	 * 
	 * Renderizza la pagina principale, con la categoria Root
	 */
	public void loadHomePage()
	{
		//setta la categoria root
		currentCategory = new Category("R",null);
		currentCategory.setId(0);
		
		loadHome(currentCategory);
	}
	
	/**
	 * 
	 * @param currentCategory categoria corrente, per renderizzare la pagina in base alla categoria
	 */
	public void loadHome(Category currentCategory)
	{
		cleanScene();
		
		this.currentCategory = currentCategory;
		
		view = new HomeView(currentCategory);
		
		//renderizza il breadcrumb e la lista delle domande
		RootPanel.get("app").add(view.renderQuestions());
		
		RootPanel.get("breadcrumb").add(view.renderBreadCrumb(currentCategory));
		
		//carica la navbar in base all'utente
		loadNavbar(currentUser);
	}
	
	public void loadNavbar(User user)
	{
		currentUser = user;
		
		//visualizza dei widget diversi in base all'utente utilizzatore
		RootPanel.get("btnLogin").add(view.renderLoginButton(user));
		RootPanel.get("btnRegister").add(view.renderRegistrationButton(user));
		
		//se l'utente è loggato mostra le categorie
		//e se non ci si trova nella categoria Home, mostra il bottone per inserire una nuova domanda
		if(currentUser != null) 
		{
			RootPanel.get("options").add(view.renderOptions());
			RootPanel.get("categories").add(view.renderCategories());
		}
	}
	
	/**
	 * 
	 * Visualizza la pagina per creare una nuova domanda
	 */
	public void questionSubmit()
	{
		cleanScene();
		RootPanel.get("app").add(new QuestionView().renderQuestionSubmit());
	}
	/**
	 * 
	 * Visualizza la pagina della domanda, con la lista delle risposte e la textarea per inserire un ulteriore risposta
	 * @param q domanda corrente 
	 */
	public void singleQuestionView(Question question)
	{
		cleanScene();
		setCurrentQuestion(question);
		RootPanel.get("app").add(new QuestionSingleView(question).render());
	}
	
	/**
	 * 
	 * Visualizza la pagina per effettuare il login
	 */
	public void login()
	{
		cleanScene();
		RootPanel.get("app").add(new UserLoginView().renderLogin());
	}
	
	/**
	 * 
	 * Visualizza la pagina per effettuare la registrazione
	 */
	public void registration()
	{
		cleanScene();
		RootPanel.get("app").add(new UserRegistrationView().renderRegistration());
	}
	
	/**
	 * 
	 * Visualizza la pagina per eleggere un giudice
	 */
	public void electJudge()
	{
		cleanScene();
		RootPanel.get("app").add(new UserElectView().render());
	}
	
	/**
	 * 
	 * Visualizza la pagina per rinominare la categoria
	 */
	public void insertCategory()
	{
		cleanScene();
		RootPanel.get("app").add(new CategoryAddView().render());
	}
	
	/**
	 * 
	 * Visualizza la pagina per rinominare la categoria
	 */
	public void renameCategory()
	{
		cleanScene();
		RootPanel.get("app").add(new CategoryRenameView().render());
	}

	/**
	 * 
	 * Ritorna l'istanza corrente della classe 
	 * @return istanza corrente SitoWeb
	 */
	public static SitoWeb getInstance() {
		return instance;
	}

	// Getter - Setter currentCategory e currentUser
	public Category getCurrentCategory() {
		return currentCategory;
	}
	
	// Setta la categoria corrente
	public void setCurrentCategory(Category currentCategory) {
		this.currentCategory = currentCategory;
	}
	
	// Ritorna lo user loggato 
	public User getCurrentUser() {
		return currentUser;
	}
	
	// Setta l'utente loggato
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
	
	// Ritorna la domanda selezionata
	public Question getCurrentQuestion() {
		return currentQuestion;
	}
	
	// Setta la domnda selezionata 
	public void setCurrentQuestion(Question currentQuestion) {
		this.currentQuestion = currentQuestion;
	}
}
