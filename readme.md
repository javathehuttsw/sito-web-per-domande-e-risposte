#Sito web per domande e risposte

###Eseguire il progetto

* Scaricare il progetto dalla [pagina di download](https://bitbucket.org/javathehuttsw/sito-web-per-domande-e-risposte/downloads) (branch master)

* Cliccare sul link "Download repository"

* Utilizzando l' IDE Eclipse selezionare dal menù a tendina : File > New > Java Project

* Deselezionare la checkbox "use default location",

* Cliccando sul bottone browse selezionare il progetto (cartella SitoWeb)

* Eseguire il progetto in "SuperDevMode"

**NB. il database nel server è già caricato** con categorie e admin (email admin@admin.com, pass admin)

Vedi anche [Manuale utente](https://bitbucket.org/javathehuttsw/sito-web-per-domande-e-risposte/wiki/Manuale%20utente)